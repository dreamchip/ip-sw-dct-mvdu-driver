/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only OR BSD-2-Clause
 */

#include "mvdu_cku.h"
#include "mvdu_functions.h"
#include "mvdu_fx_cku_reg.h"
#include "mvdu_memmap.h"
#include "mvdu_structures.h"

static void mvdu_cku_write(uint32_t val, uintptr_t base)
{
	mvdu_write(val, base + MVDU_CKU_BASE);
}

static uint32_t mvdu_cku_read(uintptr_t base)
{
	return mvdu_read(base + MVDU_CKU_BASE);
}

static uint32_t set_mask_value(uint32_t reg_value, uint32_t mask, uint32_t shift, uint32_t value)
{
	return (reg_value & ~(mask)) | ((value << shift) & mask);
}

static void mvdu_cku_write_mask(uintptr_t base, uint32_t mask, uint32_t shift, uint32_t val)
{
	uint32_t tmp = mvdu_cku_read(base);
	tmp = set_mask_value(tmp, mask, shift, val);
	mvdu_cku_write(tmp, base);
}

static void cku_set_minmax(uintptr_t base, enum key_sel key, uint32_t min, uint32_t max)
{
	switch (key) {
	case Y_KEY:
		mvdu_cku_write_mask(base + CKU_KEY_MAX_REG, Y_KEY_MAX_MASK, Y_KEY_MAX_SHIFT, max);
		mvdu_cku_write_mask(base + CKU_KEY_MIN_REG, Y_KEY_MIN_MASK, Y_KEY_MIN_SHIFT, min);
		break;
	case CB_KEY:
		mvdu_cku_write_mask(base + CKU_KEY_MAX_REG, CB_KEY_MAX_MASK, CB_KEY_MAX_SHIFT, max);
		mvdu_cku_write_mask(base + CKU_KEY_MIN_REG, CB_KEY_MIN_MASK, CB_KEY_MIN_SHIFT, min);
		break;
	case CR_KEY:
	default:
		mvdu_cku_write_mask(base + CKU_KEY_MAX_REG, CR_KEY_MAX_MASK, CR_KEY_MAX_SHIFT, max);
		mvdu_cku_write_mask(base + CKU_KEY_MIN_REG, CR_KEY_MIN_MASK, CR_KEY_MIN_SHIFT, min);
		break;
	};
}

void cku_enable(uintptr_t base, int en)
{
	mvdu_set_clear_bit(base + MVDU_CKU_BASE + CKU_CONFIG_REG, CKU_ENABLE_BIT, en);
}

void cku_config(uintptr_t base, struct cku_cfg *cku_data)
{
	uint32_t data;

	data = 0;
	set_bit(data, ENABLE_INSULAR_PADDING, cku_data->enable_insular_padding);
	set_bit(data, ENABLE_INTERPOL_PADDING, cku_data->enable_interpol_padding);
	set_bit(data, CKU_ENABLE, cku_data->cku_enable);
	mvdu_cku_write(data, base + CKU_CONFIG_REG);

	cku_set_minmax(base, Y_KEY, cku_data->y_key_min, cku_data->y_key_max);
	cku_set_minmax(base, CB_KEY, cku_data->cb_key_min, cku_data->cb_key_max);
	cku_set_minmax(base, CR_KEY, cku_data->cr_key_min, cku_data->cr_key_max);

	data = 0;
	set_slice(data, ALPHA_NO_KEY, cku_data->alpha_no_key);
	set_slice(data, ALPHA_KEY, cku_data->alpha_key);
	mvdu_cku_write(data, base + CKU_ALPHA_KEY_REG);

	data = 0;
	set_slice(data, ALPHA_INSULAR_OUT, cku_data->alpha_insular_out);
	set_slice(data, ALPHA_INSULAR_IN, cku_data->alpha_insular_in);
	set_slice(data, ALPHA_IPOL_OUT, cku_data->alpha_ipol_out);
	set_slice(data, ALPHA_IPOL_IN, cku_data->alpha_ipol_in);
	mvdu_cku_write(data, base + CKU_ALPHA_IPOL_INS_REG);
}
