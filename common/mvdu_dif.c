/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only OR BSD-2-Clause
 */

#include "mvdu_os.h"

#include "mvdu_dif.h"
#include "mvdu_functions.h"
#include "mvdu_fx_dif_reg.h"
#include "mvdu_memmap.h"
#include "mvdu_structures.h"

static void mvdu_dif_write(uint32_t val, uintptr_t base)
{
	mvdu_write(val, base + MVDU_DIF_BASE);
}

static void mvdu_config_bt656(uintptr_t base, struct mvdu_dif *dif, uint32_t h_total,
			      uint32_t v_total)
{
	uint32_t data;

	// Write sav start position
	data = 0x0;
	set_slice(data, SAV_START, dif->h_front_porch + dif->h_sync + dif->h_back_porch - 4 - 2);
	mvdu_dif_write(data, base + DIF_SAV_START_REG);

	// Write Field Start line
	data = 0x0;
	set_slice(data, EAV_F_START, v_total + 1);
	mvdu_dif_write(data, base + DIF_EAV_F_START_REG);

	// Write Hsync Start Position
	// not used registers in this mode
	data = 0x0;
	set_slice(data, HSYNC_START, h_total + 1);
	mvdu_dif_write(data, base + DIF_HSYNC_START_REG);

	// Write H_sync Stop Position
	data = 0x0;
	set_slice(data, HSYNC_STOP, h_total + 1);
	mvdu_dif_write(data, base + DIF_HSYNC_STOP_REG);

	// Write V_sync Start Position
	data = 0;
	set_slice(data, VSYNC_VER_START, v_total + 1);
	set_slice(data, VSYNC_HOR_START, h_total + 1);
	mvdu_dif_write(data, base + DIF_VSYNC_START_REG);

	// Write V_sync Stop Position
	data = 0;
	set_slice(data, VSYNC_VER_STOP, v_total + 1);
	set_slice(data, VSYNC_HOR_STOP, h_total + 1);
	mvdu_dif_write(data, base + DIF_VSYNC_STOP_REG);

	// Write Picture Start Paras
	data = 0x0;
	set_slice(data, PIC_VER_START, dif->v_sync + dif->v_back_porch);
	set_slice(data, PIC_HOR_START, dif->h_front_porch + dif->h_sync + dif->h_back_porch - 4);
	mvdu_dif_write(data, base + DIF_PIC_START_REG);

	// Write Picture Stop Paras
	data = 0;
	set_slice(data, PIC_VER_STOP, dif->v_sync + dif->v_back_porch + dif->act_height - 1);
	mvdu_dif_write(data, base + DIF_PIC_STOP_REG);

	data = 0x0;
	set_slice(data, DISP_FINISH_VER_POS, v_total - 1);
	set_slice(data, DISP_FINISH_HOR_POS, h_total - 4 - 1);
	mvdu_dif_write(data, base + DIF_DISP_FINISH_POS_REG);

	// Write Pos_max, length of line
	data = 0x0;
	set_slice(data, POS_MAX, h_total - 4 - 1);
	mvdu_dif_write(data, base + DIF_POS_MAX_REG);
}

static void mvdu_config_bt601(uintptr_t base, struct mvdu_dif *dif, uint32_t h_total,
			      uint32_t v_total)
{
	uint32_t data;

	// Write sav start position
	data = 0x0;
	set_slice(data, SAV_START, dif->h_front_porch + dif->h_sync + dif->h_back_porch - 4);
	mvdu_dif_write(data, base + DIF_SAV_START_REG);

	// Write Field Start line
	// outside of the field
	data = 0x0;
	set_slice(data, EAV_F_START, h_total + 1);
	mvdu_dif_write(data, base + DIF_EAV_F_START_REG);

	// Write Hsync Start Position
	data = 0x0;
	set_slice(data, HSYNC_START, dif->h_front_porch - 1);
	mvdu_dif_write(data, base + DIF_HSYNC_START_REG);

	// Write H_sync Stop Position
	data = 0x0;
	set_slice(data, HSYNC_STOP, dif->h_front_porch + dif->h_sync - 1);
	mvdu_dif_write(data, base + DIF_HSYNC_STOP_REG);

	// Write V_sync Start Position
	data = 0;
	set_slice(data, VSYNC_VER_START, 0);
	set_slice(data, VSYNC_HOR_START, dif->h_front_porch - 1);
	mvdu_dif_write(data, base + DIF_VSYNC_START_REG);

	// Write V_sync Stop Position
	data = 0;
	set_slice(data, VSYNC_VER_STOP, dif->v_sync);
	set_slice(data, VSYNC_HOR_STOP, dif->h_front_porch - 1);
	mvdu_dif_write(data, base + DIF_VSYNC_STOP_REG);

	// Write Picture Start Paras
	data = 0x0;
	set_slice(data, PIC_VER_START, dif->v_sync + dif->v_back_porch);
	set_slice(data, PIC_HOR_START, dif->h_front_porch + dif->h_sync + dif->h_back_porch);
	mvdu_dif_write(data, base + DIF_PIC_START_REG);

	// Write Picture Stop Paras
	data = 0;
	set_slice(data, PIC_VER_STOP, dif->v_sync + dif->v_back_porch + dif->act_height - 1);
	mvdu_dif_write(data, base + DIF_PIC_STOP_REG);

	data = 0x0;
	set_slice(data, DISP_FINISH_VER_POS, v_total - 1);
	set_slice(data, DISP_FINISH_HOR_POS, h_total - 1);
	mvdu_dif_write(data, base + DIF_DISP_FINISH_POS_REG);

	// Write Pos_max, length of line
	data = 0x0;
	set_slice(data, POS_MAX, h_total - 1);
	mvdu_dif_write(data, base + DIF_POS_MAX_REG);
}

void mvdu_config_display(uintptr_t base, struct mvdu_dif *dif)
{
	uint32_t data;
	uint32_t h_total = dif->act_width + dif->h_front_porch + dif->h_sync + dif->h_back_porch;
	uint32_t v_total = dif->act_height + dif->v_front_porch + dif->v_sync + dif->v_back_porch;
	uint32_t eav_f_init = 0; // init value for eav_f signal

	// Clipping Values
	data = 0x0;
	set_slice(data, CMAX, 0xfe);
	set_slice(data, CMIN, 0x01);
	set_slice(data, YMAX, 0xfe);
	set_slice(data, YMIN, 0x01);
	mvdu_dif_write(data, base + DIF_CLIPPING_REG);

	// Polarity  Values for sync signals ; 1-> low active
	data = 0x0;
	set_bit(data, INTERLACED_MODE, dif->interlaced);
	set_bit(data, VS_POLARITY, dif->vsync_polarity);
	set_bit(data, HS_POLARITY, dif->hsync_polarity);
	set_bit(data, FSYNC_POLARITY, dif->fsync_polarity);
	set_bit(data, PEN_POLARITY, dif->pen_polarity);
	mvdu_dif_write(data, base + DIF_CONFIG_REG);

	data = 0x0;
	mvdu_dif_write(data, base + DIF_LINE_CNT_THRES_REG);

	// Default Display  Values
	data = 0x0;
	set_slice(data, DEFAULT_CB, dif->def_cb);
	set_slice(data, DEFAULT_CR, dif->def_cr);
	set_slice(data, DEFAULT_Y, dif->def_y);
	mvdu_dif_write(data, base + DIF_DEFAULT_COL_REG);

	data = 0x0;
	mvdu_dif_write(data, base + DIF_FRAM_START_REG);

	// Path Enables
	data = 0x0;
	set_slice(data, OUTPUT_MODE, dif->timing_mode);
	mvdu_dif_write(data, base + DIF_PATH_ENABLE_REG);

	// Write line_max,
	data = 0x0;
	set_slice(data, LINE_MAX, v_total - 1);
	mvdu_dif_write(data, base + DIF_LINE_MAX_REG);

	// Write Field Stop line
	// outside of the field
	data = 0x0;
	set_slice(data, EAV_F_STOP, v_total + 1);
	mvdu_dif_write(data, base + DIF_EAV_F_STOP_REG);

	// Write F_sync Start Position
	data = 0;
	set_slice(data, FSYNC_VER_START, v_total + 1);
	set_slice(data, FSYNC_HOR_START, h_total + 1);
	mvdu_dif_write(data, base + DIF_FSYNC_START_REG);

	// Write F_sync Stop Position
	data = 0;
	set_slice(data, FSYNC_VER_STOP, v_total + 1);
	set_slice(data, FSYNC_HOR_STOP, h_total + 1);
	mvdu_dif_write(data, base + DIF_FSYNC_STOP_REG);

	// Write init values
	data = 0;
	set_bit(data, EAV_F_INIT, eav_f_init);
	mvdu_dif_write(data, base + DIF_INIT_REG);

	data = 0x0;
	mvdu_dif_write(data, base + DIF_FRAM_START_REG);

	if (dif->timing_mode == TM_YCMUX_EMSYNC_2REPL || dif->timing_mode == TM_YC_EMSYNC_1REPL) {
		mvdu_config_bt656(base, dif, h_total, v_total);
	} else {
		mvdu_config_bt601(base, dif, h_total, v_total);
	}
}

void mvdu_dif_frame_start(uintptr_t base, bool enable)
{
	mvdu_set_clear_bit(base + MVDU_DIF_BASE + DIF_FRAM_START_REG, FRAM_START_EN_BIT, enable);
}

void mvdu_update_display_start_pos(uintptr_t base, struct mvdu_dif *dif)
{
	uint32_t data;

	// Write Video start Pos
	data = 0;
	set_slice(data, ACTIVE_VER_START, dif->act_voffset);
	set_slice(data, ACTIVE_HOR_START, dif->act_hoffset >> 1);
	mvdu_dif_write(data, base + DIF_ACTIVE_START_REG);

	// Write PIP start Pos
	data = 0;
	set_slice(data, PIP_VER_START, dif->pip_voffset);
	set_slice(data, PIP_HOR_START, dif->pip_hoffset >> 1);
	mvdu_dif_write(data, base + DIF_PIP_START_REG);
}