/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only OR BSD-2-Clause
 */

#include "mvdu_os.h"

#include "gdp_ctrl_reg.h"
#include "gdp_memmap.h"
#include "gdp_mi_reg.h"
#include "mvdu_functions.h"
#include "mvdu_gdp.h"
#include "mvdu_structures.h"

static uint32_t gdp_byte_per_pixel(uint32_t type)
{
	switch (type) {
	case RGB565:
		return 2;
		break;
	case RGB888:
		return 3;
		break;
	case RGBA8888:
		return 4;
		break;
	case RGBA4444:
		return 2;
		break;
	case RGBA6666:
		return 3;
		break;
	case RGBS5551:
		return 2;
		break;
	case RGBS8871:
		return 3;
		break;
	}
	return 0;
}

static void mvdu_fill_zone(uint8_t *buf, struct zone_desc *zone)
{
	if (zone == NULL) {
		memset(buf, 0, 16);
		return;
	}

	buf[0] = zone->buf_stride & 0xff;
	buf[1] = (zone->buf_stride >> 8) & 0xff;
	buf[2] = zone->line_bytes & 0xff;
	buf[3] = (zone->line_bytes >> 8) & 0xff;
	buf[4] = 0x00;
	buf[5] = zone->num_of_zones_in_stripe & 0xff;
	buf[6] = zone->last_y & 0xff;
	buf[7] = (zone->last_y >> 8) & 0xff;
	buf[8] = zone->num_pixels & 0xff;
	buf[9] = (zone->num_pixels >> 8) & 0xff;
	buf[10] = zone->alpha & 0xff;
	buf[11] = zone->type & 0xff;
	buf[12] = zone->buf_origin & 0xff;
	buf[13] = (zone->buf_origin >> 8) & 0xff;
	buf[14] = (zone->buf_origin >> 16) & 0xff;
	buf[15] = (zone->buf_origin >> 24) & 0xff;
}

void mvdu_config_zones(struct mvdu_mif *mif, uintptr_t zone_base_virt, uintptr_t rgb_base)
{
	struct zone_desc zone;
	uint8_t *ptr;
	uint8_t num_of_zones_in_stripe;
	uint8_t hidden = 0x00;

	if (!zone_base_virt)
		return;

	zone.type = RGBA8888;
	zone.buf_stride = mif->y.full_width * gdp_byte_per_pixel(zone.type);

	ptr = (uint8_t *)zone_base_virt;

	if (mif->y.top) {
		zone.alpha = hidden;
		zone.last_y = mif->y.top - 1;
		zone.num_pixels = mif->y.full_width;
		zone.num_of_zones_in_stripe = 1;
		zone.line_bytes = zone.num_pixels * gdp_byte_per_pixel(zone.type);
		zone.buf_origin = rgb_base;
		mvdu_fill_zone(ptr, &zone);
		ptr += 16;
		rgb_base += mif->y.top * zone.buf_stride;
	}

	num_of_zones_in_stripe = 1;
	if (mif->y.left)
		num_of_zones_in_stripe++;
	if ((mif->y.left + mif->y.width) < mif->y.full_width)
		num_of_zones_in_stripe++;

	zone.last_y = mif->y.top + mif->y.height - 1;

	if (mif->y.left) {
		zone.alpha = hidden;
		zone.num_pixels = mif->y.left;
		zone.num_of_zones_in_stripe = num_of_zones_in_stripe;
		num_of_zones_in_stripe = 0;
		zone.line_bytes = zone.num_pixels * gdp_byte_per_pixel(zone.type);
		zone.buf_origin = rgb_base;
		mvdu_fill_zone(ptr, &zone);
		ptr += 16;
		rgb_base += zone.line_bytes;
	}

	if (mif->y.width) {
		zone.alpha = mif->alpha;
		zone.num_pixels = mif->y.width;
		zone.num_of_zones_in_stripe = num_of_zones_in_stripe;
		num_of_zones_in_stripe = 0;
		zone.line_bytes = zone.num_pixels * gdp_byte_per_pixel(zone.type);
		zone.buf_origin = rgb_base;
		mvdu_fill_zone(ptr, &zone);
		ptr += 16;
		rgb_base += zone.line_bytes;
	}

	if ((mif->y.left + mif->y.width) < mif->y.full_width) {
		zone.alpha = hidden;
		zone.num_pixels = mif->y.full_width - mif->y.width - mif->y.left;
		zone.num_of_zones_in_stripe = num_of_zones_in_stripe;
		zone.line_bytes = zone.num_pixels * gdp_byte_per_pixel(zone.type);
		zone.buf_origin = rgb_base;
		mvdu_fill_zone(ptr, &zone);
		ptr += 16;
		rgb_base += zone.line_bytes;
	}

	rgb_base += mif->y.height * zone.buf_stride - zone.buf_stride;

	num_of_zones_in_stripe = 1;

	if ((mif->y.top + mif->y.height) < mif->y.full_height) {
		zone.alpha = hidden;
		zone.last_y = mif->y.full_height - 1;
		zone.num_pixels = mif->y.full_width;
		zone.num_of_zones_in_stripe = num_of_zones_in_stripe;
		zone.line_bytes = zone.num_pixels * gdp_byte_per_pixel(zone.type);
		zone.buf_origin = rgb_base;
		mvdu_fill_zone(ptr, &zone);
		ptr += 16;
	}

	mvdu_fill_zone(ptr, NULL);
}

void mvdu_config_mif_osd(uintptr_t gdp_base, uintptr_t zone_base)
{
	if (!zone_base)
		return;

	mvdu_write(zone_base, gdp_base + GDP_MI_BASE + GDP_MI_SCL_START_ADDR_REG);
}

void mvdu_enable_gdp(uintptr_t gdp_base)
{
	mvdu_set_bit(gdp_base + GDP_CTRL_BASE + GDP_CONTROL_REG, TG_ENABLE_BIT);
}

void mvdu_disable_gdp(uintptr_t gdp_base)
{
	mvdu_clear_bit(gdp_base + GDP_CTRL_BASE + GDP_CONTROL_REG, TG_ENABLE_BIT);
}
