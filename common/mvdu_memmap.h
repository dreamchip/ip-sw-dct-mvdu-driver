/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only OR BSD-2-Clause
 */

#ifndef __MVDU_MEMMAP_H__
#define __MVDU_MEMMAP_H__

#define MVDU_CTRL_BASE 0x0000
#define MVDU_RSZ_VID1_BASE 0x0100
#define MVDU_RSZ_PIP_BASE 0x0200
#define MVDU_DIF_BASE 0x0400
#define MVDU_OSD_BASE 0x0600
#define MVDU_MI_BASE 0x0900
#define MVDU_CKU_BASE 0x0B00
#define MVDU_CCU_VID1_BASE 0x0C00
#define MVDU_CCU_PIP_BASE 0x0D00

#endif
