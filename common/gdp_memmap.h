/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only OR BSD-2-Clause
 */

#ifndef _GDP_MEMMAP_H
#define _GDP_MEMMAP_H

#define GDP_CTRL_BASE 0x000
#define GDP_DIF_BASE 0x100
#define GDP_MI_BASE 0x200

#endif // _GDP_MEMMAP_H
