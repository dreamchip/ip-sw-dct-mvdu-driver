/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only OR BSD-2-Clause
 */

#ifndef __MVDU_OS_H__
#define __MVDU_OS_H__

#if defined(__ZEPHYR__)
#include <zephyr/ztest.h>
#else
#include <linux/io.h>
#endif

#endif // __MVDU_OS_H__
