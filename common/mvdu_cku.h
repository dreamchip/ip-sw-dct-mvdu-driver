/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only OR BSD-2-Clause
 */

#ifndef __MVDU_CKU_H__
#define __MVDU_CKU_H__

#include "mvdu_os.h"

enum key_sel { Y_KEY, CB_KEY, CR_KEY };

struct cku_cfg {
	uint32_t enable_insular_padding;
	uint32_t enable_interpol_padding;
	uint32_t cku_enable;
	uint32_t cr_key_max;
	uint32_t cb_key_max;
	uint32_t y_key_max;
	uint32_t cr_key_min;
	uint32_t cb_key_min;
	uint32_t y_key_min;
	uint32_t alpha_no_key;
	uint32_t alpha_key;
	uint32_t alpha_insular_out;
	uint32_t alpha_insular_in;
	uint32_t alpha_ipol_out;
	uint32_t alpha_ipol_in;
};

void cku_enable(uintptr_t base, int en);
void cku_config(uintptr_t base, struct cku_cfg *data);

#endif
