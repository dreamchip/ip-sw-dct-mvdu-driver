/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only OR BSD-2-Clause
 */

#ifndef __MVDU_FUNCTIONS_H__
#define __MVDU_FUNCTIONS_H__

#include "mvdu_os.h"
#include "mvdu_structures.h"

uint32_t mvdu_read(uintptr_t base);
void mvdu_write(uint32_t val, uintptr_t base);
uint32_t mvdu_read_mask(uintptr_t base, uint32_t mask, uint32_t shift);
void mvdu_write_mask(uintptr_t base, uint32_t mask, uint32_t shift, uint32_t val);
void mvdu_set_bit(uintptr_t base, uint32_t bit);
void mvdu_clear_bit(uintptr_t base, uint32_t bit);
void mvdu_set_clear_bit(uintptr_t base, uint32_t bit, bool enable);

void mvdu_start_vtg(uintptr_t base);
void mvdu_stop_vtg(uintptr_t base);

void mvdu_enable_video(uintptr_t base);
void mvdu_disable_video(uintptr_t base);

void mvdu_enable_pip(uintptr_t base);
void mvdu_disable_pip(uintptr_t base);

void mvdu_enable_osd(uintptr_t mvdu, uintptr_t gdp);
void mvdu_disable_osd(uintptr_t mvdu, uintptr_t gdp);

void mvdu_enable_disp_finish_irq(uintptr_t base);
void mvdu_disable_disp_finish_irq(uintptr_t base);
void mvdu_enable_line_threshold_irq(uintptr_t base);
void mvdu_disable_line_threshold_irq(uintptr_t base);

#define set_slice(reg, name, value)                                                                \
	{                                                                                          \
		((reg) = (((reg) & ~(name##_MASK)) |                                               \
			  (((value) << (name##_SHIFT)) & (name##_MASK))));                         \
	}

#define set_bit(reg, name, value)                                                                  \
	{                                                                                          \
		((reg) = (value) ? ((reg) | (name##_MASK)) : ((reg) & ~(name##_MASK)));            \
	}

#endif
