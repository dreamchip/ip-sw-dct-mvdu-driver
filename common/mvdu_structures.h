/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only OR BSD-2-Clause
 */

#ifndef __MVDU_STRUCTURES_H__
#define __MVDU_STRUCTURES_H__

#include "mvdu_os.h"

//! polarity types
enum mvdu_polarity { HIGH_ACTIVE, LOW_ACTIVE };

//! timing modes of display interface
enum mvdu_timing {
	TM_YCMUX_EMSYNC_2REPL = 0x0, //!< 000: YC MUX 4:2:2 with embedded syncs
	//(ITU-R BT.656), 8 bit, 2 clk/pix
	TM_YC_EXSYNC_1REPL = 0x1, //!< 001: YC 4:2:2 with explicit (separate)
	// syncs, 16 bit, 1 clk/pix
	TM_YCMUX_EXSYNC_2REPL = 0x2, //!< 010: YC MUX 4:2:2 with explicit
	//(separate) syncs, 8 bit, 2 clk/pix
	TM_YC_EMSYNC_1REPL = 0x3, //!< 011: YC 4:2:2 with embedded syncs, 16 bit, 1 clk/pix
	TM_YC_EXSYNC_2REPL = 0x4  //!< 100: YC 4:2:2 with explicit (separate)
				  // syncs, 16 bit, 2 clk/pix
};

struct mvdu_plane {
	uint32_t width;
	uint32_t height;
	uint32_t top;
	uint32_t left;
	uint32_t full_width;
	uint32_t full_height;
};

struct mvdu_top {
	uint32_t omux_ctrl;
};

struct zone_mem {
	uintptr_t paddr;
	void *vaddr;
	unsigned int size;
};

struct zone_desc {
	uint16_t buf_stride;
	uint16_t line_bytes;
	uint8_t dummy;
	uint8_t num_of_zones_in_stripe;
	uint16_t last_y;
	uint32_t num_pixels;
	uint8_t alpha;
	uint8_t type;
	uint32_t buf_origin;
};

#define RGB565 (0x6)
#define RGB888 (0x7)
#define RGBA8888 (0x8)
#define RGBA4444 (0xa)
#define RGBA6666 (0xb)
#define RGBS5551 (0xe)
#define RGBS8871 (0xf)

#endif
