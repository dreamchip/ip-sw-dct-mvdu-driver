/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only OR BSD-2-Clause
 */

#ifndef __MVDU_MI_H__
#define __MVDU_MI_H__

#include "mvdu_os.h"
#include "mvdu_structures.h"

/*!
 * \brief memory interface
 */
struct mvdu_mif {
	struct mvdu_plane y;
	struct mvdu_plane c;
	uint32_t urgent_thresh;
	uint32_t range_reduct;
	uint32_t range_map_flag;
	uint32_t range_map;
	uint32_t small_packet_en;
	uint32_t mb_mapping;
	uint32_t little_endian;
	uint32_t color_mode_422;
	uint32_t field_access;
	uint32_t alpha;
};

void mvdu_config_mif_video(uintptr_t base, struct mvdu_mif *mif);
void mvdu_config_mif_pip(uintptr_t base, struct mvdu_mif *mif);

void mvdu_mi_write(uint32_t val, uintptr_t base);
uint32_t mvdu_mi_read(uintptr_t base);

#endif
