/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only OR BSD-2-Clause
 */

#ifndef __MVDU_DIF_H__
#define __MVDU_DIF_H__

#include "mvdu_os.h"
#include "mvdu_structures.h"

struct mvdu_dif {
	uint32_t act_hoffset;
	uint32_t act_voffset;
	uint32_t act_width;
	uint32_t act_height;
	uint32_t h_front_porch;
	uint32_t h_sync;
	uint32_t h_back_porch;
	uint32_t v_front_porch;
	uint32_t v_sync;
	uint32_t v_back_porch;
	uint32_t pip_hoffset;
	uint32_t pip_voffset;
	uint32_t def_y;
	uint32_t def_cb;
	uint32_t def_cr;
	uint32_t pixelclock;
	enum mvdu_timing timing_mode; //!< timing mode of display interface
	uint32_t vsync_polarity;
	uint32_t hsync_polarity;
	uint32_t fsync_polarity;
	uint32_t pen_polarity;
	uint32_t interlaced;
};

void mvdu_config_display(uintptr_t base, struct mvdu_dif *dif);
void mvdu_update_display_start_pos(uintptr_t base, struct mvdu_dif *dif);
void mvdu_dif_frame_start(uintptr_t base, bool enable);

#endif
