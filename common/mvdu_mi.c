/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only OR BSD-2-Clause
 */

#include "mvdu_os.h"

#include "mvdu_functions.h"
#include "mvdu_fx_mi_reg.h"
#include "mvdu_memmap.h"
#include "mvdu_mi.h"
#include "mvdu_structures.h"

void mvdu_config_mif_video(uintptr_t base, struct mvdu_mif *mif)
{
	uint32_t data;

	mvdu_mi_write(mif->y.height, base + MI_V1_Y_NR_OF_ROWS_REG);
	mvdu_mi_write(mif->c.height, base + MI_V1_C_NR_OF_ROWS_REG);
	mvdu_mi_write(mif->y.width, base + MI_V1_Y_LINE_SIZE_REG);
	mvdu_mi_write(mif->c.width, base + MI_V1_C_LINE_SIZE_REG);
	mvdu_mi_write(mif->y.full_width, base + MI_V1_Y_FULL_LINE_SIZE_REG);
	mvdu_mi_write(mif->c.full_width, base + MI_V1_C_FULL_LINE_SIZE_REG);

	data = 0;
	set_slice(data, V1_Y_X_POS, mif->y.left);
	set_slice(data, V1_Y_Y_POS, mif->y.top);
	mvdu_mi_write(data, base + MI_V1_Y_START_POS_REG);

	data = 0;
	set_slice(data, V1_C_X_POS, mif->c.left);
	set_slice(data, V1_C_Y_POS, mif->c.top);
	mvdu_mi_write(data, base + MI_V1_C_START_POS_REG);

	data = 0;
	//	set_slice(data, V1_URGENT_THRESH, mif->urgent_thresh);
	//	set_slice(data, V1_RANGE_REDUCT_FLAG, mif->range_reduct);
	//	set_slice(data, V1_RANGE_MAPUV, mif->range_map);
	//	set_slice(data, V1_RANGE_MAPUV_FLAG, mif->range_map_flag);
	//	set_slice(data, V1_RANGE_MAPY, mif->range_map);
	//	set_slice(data, V1_RANGE_MAPY_FLAG, mif->range_map_flag);
	//	set_slice(data, V1_SMALL_PACKET_EN, mif->small_packet_en);
	//	set_slice(data, V1_MAPPING_TYPE, mif->mb_mapping);
	set_bit(data, V1_LITTLE_ENDIAN, mif->little_endian);
	//	set_slice(data, V1_COLOR_MODE, mif->color_mode_422);
	//	set_slice(data, V1_FIELD_ACCESS, mif->field_access);
	mvdu_mi_write(data, base + MI_V1_CONFIG_REG);

	data = 0;
	mvdu_mi_write(data, base + MI_V1_PIP_IN_DELAY_COUNT_THRESH_REG);
}

void mvdu_config_mif_pip(uintptr_t base, struct mvdu_mif *mif)
{
	uint32_t data;

	mvdu_mi_write(mif->y.height, base + MI_PIP_IN_Y_NR_OF_ROWS_REG);
	mvdu_mi_write(mif->c.height, base + MI_PIP_IN_C_NR_OF_ROWS_REG);
	mvdu_mi_write(mif->y.width, base + MI_PIP_IN_Y_LINE_SIZE_REG);
	mvdu_mi_write(mif->c.width, base + MI_PIP_IN_C_LINE_SIZE_REG);
	mvdu_mi_write(mif->y.full_width, base + MI_PIP_IN_Y_FULL_LINE_SIZE_REG);
	mvdu_mi_write(mif->c.full_width, base + MI_PIP_IN_C_FULL_LINE_SIZE_REG);

	data = 0;
	set_slice(data, PIP_IN_Y_X_POS, mif->y.left);
	set_slice(data, PIP_IN_Y_Y_POS, mif->y.top);
	mvdu_mi_write(data, base + MI_PIP_IN_Y_START_POS_REG);

	data = 0;
	set_slice(data, PIP_IN_C_X_POS, mif->c.left);
	set_slice(data, PIP_IN_C_Y_POS, mif->c.top);
	mvdu_mi_write(data, base + MI_PIP_IN_C_START_POS_REG);

	data = 0;
	//	set_slice(data, PIP_IN_URGENT_THRESH, mif->urgent_thresh);
	//	set_slice(data, PIP_IN_RANGE_REDUCT_FLAG, mif->range_reduct);
	//	set_slice(data, PIP_IN_RANGE_MAPUV, mif->range_map);
	//	set_slice(data, PIP_IN_RANGE_MAPUV_FLAG, mif->range_map_flag);
	//	set_slice(data, PIP_IN_RANGE_MAPY, mif->range_map);
	//	set_slice(data, PIP_IN_RANGE_MAPY_FLAG, mif->range_map_flag);
	//	set_slice(data, PIP_IN_SMALL_PACKET_EN, mif->small_packet_en);
	//	set_slice(data, PIP_IN_MAPPING_TYPE, mif->mb_mapping);
	set_bit(data, PIP_IN_LITTLE_ENDIAN, mif->little_endian);
	//	set_slice(data, PIP_IN_COLOR_MODE, mif->color_mode_422);
	//	set_slice(data, PIP_IN_FIELD_ACCESS, mif->field_access);
	mvdu_mi_write(data, base + MI_PIP_IN_CONFIG_REG);
}

void mvdu_mi_write(uint32_t val, uintptr_t base)
{
	mvdu_write(val, base + MVDU_MI_BASE);
}

uint32_t mvdu_mi_read(uintptr_t base)
{
	return mvdu_read(base + MVDU_MI_BASE);
}
