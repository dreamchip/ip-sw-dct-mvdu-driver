/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only OR BSD-2-Clause
 */

#ifndef __MVDU_GDP_H__
#define __MVDU_GDP_H__

#include "mvdu_os.h"
#include "mvdu_structures.h"

#include "mvdu_mi.h"

void mvdu_config_zones(struct mvdu_mif *mif, uintptr_t zone_base_virt, uintptr_t rgb_base);
void mvdu_config_mif_osd(uintptr_t gdp_base, uintptr_t zone_base_phys);

void mvdu_enable_gdp(uintptr_t gdp_ctrl_base);
void mvdu_disable_gdp(uintptr_t gdp_ctrl_base);

#endif
