/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only OR BSD-2-Clause
 */

#include "mvdu_os.h"

#include "mvdu_functions.h"
#include "mvdu_fx_ctrl_reg.h"
#include "mvdu_fx_dif_reg.h"
#include "mvdu_fx_mi_reg.h"
#include "mvdu_fx_osd_reg.h"
#include "mvdu_gdp.h"
#include "mvdu_dif.h"
#include "mvdu_mi.h"
#include "mvdu_memmap.h"
#include "mvdu_structures.h"

#define HL(a, b) (((a) << 16) | (b))

void mvdu_start_vtg(uintptr_t base)
{
	mvdu_dif_frame_start(base, true);

	mvdu_write(DIF_VID1_UFLW_CLR_MASK | DIF_OSD1_UFLW_CLR_MASK | DIF_PIP_UFLW_CLR_MASK,
		   base + MVDU_ISR_REG);

	mvdu_write(STOP_VTG_MASK, base + MVDU_CONTROL_REG);
	// msleep(1);
	mvdu_write(START_VTG_MASK, base + MVDU_CONTROL_REG);

	mvdu_write(DIF_VID1_UFLW_CLR_MASK | DIF_OSD1_UFLW_CLR_MASK | DIF_PIP_UFLW_CLR_MASK,
		   base + MVDU_ICR_REG);

	mvdu_dif_frame_start(base, false);
}

void mvdu_stop_vtg(uintptr_t base)
{
	uint32_t ris, cnt = 0;

	ris = mvdu_read(base + MVDU_RIS_REG);

	if (ris & BIT(11)) {
		printk("ERROR: VTG interrupt still present\n");
	}

	mvdu_write(STOP_VTG_MASK, base + MVDU_CONTROL_REG);

	do {
		ris = mvdu_read(base + MVDU_RIS_REG);
		if (ris & BIT(11))
			break;
		// msleep(10);
	} while (cnt++ < 100);

	if (cnt >= 100) {
		printk("ERROR: missing VTG interrupt\n");
	}
}

void mvdu_enable_video(uintptr_t base)
{
	mvdu_set_bit(base + DATAPATH_CONFIG_REG, SET_ENABLE_VID1_BIT);
}

void mvdu_enable_pip(uintptr_t base)
{
	mvdu_set_bit(base + DATAPATH_CONFIG_REG, SET_ENABLE_PIP_BIT);
}

void mvdu_enable_osd(uintptr_t mvdu_base, uintptr_t gdp_base)
{
	mvdu_set_bit(mvdu_base + DATAPATH_CONFIG_REG, SET_ENABLE_OSD1_BIT);
	mvdu_enable_gdp(gdp_base);
}

void mvdu_disable_video(uintptr_t base)
{
	mvdu_clear_bit(base + DATAPATH_CONFIG_REG, SET_ENABLE_VID1_BIT);
}

void mvdu_disable_pip(uintptr_t base)
{
	mvdu_clear_bit(base + DATAPATH_CONFIG_REG, SET_ENABLE_PIP_BIT);
}

void mvdu_disable_osd(uintptr_t mvdu, uintptr_t gdp_base)
{
	mvdu_clear_bit(mvdu + DATAPATH_CONFIG_REG, SET_ENABLE_OSD1_BIT);
	mvdu_disable_gdp(gdp_base);
}

void mvdu_enable_disp_finish_irq(uintptr_t base)
{
	//mvdu_set_bit(base + MVDU_MSK_REG, DIF_DISP_FINISH_MSK_BIT);
	uint32_t mask = PIPIN_MEM_URGENT_MSK_MASK | VID1_MEM_URGENT_MSK_MASK |
			DIF_OSD1_UFLW_MSK_MASK | DIF_PIP_UFLW_MSK_MASK | DIF_VID1_UFLW_MSK_MASK |
			DIF_LINE_TRESHOLD_MSK_MASK | PIPOUT_FINISH_MSK_MASK |
			DIF_DISP_FINISH_MSK_MASK;

	mvdu_write(mask, base + MVDU_MSK_REG);
}

void mvdu_disable_disp_finish_irq(uintptr_t base)
{
	mvdu_clear_bit(base + MVDU_MSK_REG, DIF_DISP_FINISH_MSK_BIT);
}

void mvdu_enable_line_threshold_irq(uintptr_t base)
{
	mvdu_set_bit(base + MVDU_MSK_REG, DIF_LINE_TRESHOLD_MSK_BIT);
}

void mvdu_disable_line_threshold_irq(uintptr_t base)
{
	mvdu_clear_bit(base + MVDU_MSK_REG, DIF_LINE_TRESHOLD_MSK_BIT);
}

void mvdu_enable_video_mem_urgent_irq(uintptr_t base)
{
	mvdu_set_bit(base + MVDU_MSK_REG, VID1_MEM_URGENT_MSK_BIT);
}

void mvdu_enable_pip_mem_urgent_irq(uintptr_t base)
{
	mvdu_set_bit(base + MVDU_MSK_REG, PIPIN_MEM_URGENT_MSK_BIT);
}

void mvdu_disable_video_mem_urgent_irq(uintptr_t base)
{
	mvdu_clear_bit(base + MVDU_MSK_REG, VID1_MEM_URGENT_MSK_BIT);
}

void mvdu_disable_pip_mem_urgent_irq(uintptr_t base)
{
	mvdu_clear_bit(base + MVDU_MSK_REG, PIPIN_MEM_URGENT_MSK_BIT);
}

static uint32_t get_mask_value(uint32_t value, uint32_t mask, uint32_t shift)
{
	return (value & mask) >> shift;
}

static uint32_t set_mask_value(uint32_t reg_value, uint32_t mask, uint32_t shift, uint32_t value)
{
	return (reg_value & ~(mask)) | ((value << shift) & mask);
}

void mvdu_write_mask(uintptr_t base, uint32_t mask, uint32_t shift, uint32_t val)
{
	uint32_t tmp = mvdu_read(base);
	tmp = set_mask_value(tmp, mask, shift, val);
	mvdu_write(tmp, base);
}

uint32_t mvdu_read_mask(uintptr_t base, uint32_t mask, uint32_t shift)
{
	uint32_t tmp = mvdu_read(base);
	return get_mask_value(tmp, mask, shift);
}

void mvdu_set_bit(uintptr_t base, uint32_t bit)
{
	uint32_t tmp = mvdu_read(base);
	mvdu_write(tmp | BIT(bit), base);
}

void mvdu_clear_bit(uintptr_t base, uint32_t bit)
{
	uint32_t tmp = mvdu_read(base);
	mvdu_write(tmp & ~BIT(bit), base);
}

void mvdu_set_clear_bit(uintptr_t base, uint32_t bit, bool enable)
{
	if (enable)
		mvdu_set_bit(base, bit);
	else
		mvdu_clear_bit(base, bit);
}
