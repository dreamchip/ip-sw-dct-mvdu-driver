/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#define DT_DRV_COMPAT dct_mvdu

#include <zephyr/device.h>
#include <zephyr/kernel.h>
#include <zephyr/logging/log.h>

#include <video.h>
#include <dct_mvdu.h>

#include <mvdu_cku.h>
#include <mvdu_functions.h>
#include <mvdu_memmap.h>
#include <gdp_memmap.h>
#include <mvdu_structures.h>

#include <gdp_ctrl_reg.h>
#include <mvdu_dif.h>
#include <mvdu_fx_ctrl_reg.h>
#include <mvdu_fx_dif_reg.h>
#include <mvdu_fx_mi_reg.h>
#include <mvdu_fx_osd_reg.h>
#include <mvdu_gdp.h>
#include <mvdu_mi.h>
#include <mvdu_top_reg.h>

LOG_MODULE_REGISTER(mvdu /*, LOG_LEVEL_DBG*/);

#define MIN_WIDTH 16
#define MIN_HEIGHT 16
#define MAX_WIDTH 1920
#define MAX_HEIGHT 1200
#define ALIGN_W 3
#define ALIGN_H 3
#define MIN_PIXELCLOCK 20000000
#define MAX_PIXELCLOCK 225000000

static const struct bt_timings default_timing = DV_BT_CEA_1920X1080P30;

struct mvdu_stream_data {
	struct mvdu_mif mif;
	int (*irq_handler)(uint32_t idx, void *irq_ctx);
	void *irq_ctx;
	bool enabled;
	struct vrect compose;
	struct vrect crop;
	struct video_format input_format;
	uint8_t alpha;
};

struct mvdu_runtime {
	uintptr_t fx_base;
	uintptr_t gdp_base;
	uintptr_t top_base;
	struct mvdu_config cfg;
	struct mvdu_mif mif;
	struct mvdu_dif dif;
	struct mvdu_top top;
	struct mvdu_stream_data stream[MAX_STREAMS];
	struct mvdu_stat stat;
	struct bt_timings timing;
	uint32_t bgcolor;
};

static const struct mvdu_dif def_display_info = {

	/* timing mode of display interface */
	.timing_mode = TM_YC_EXSYNC_1REPL,

	.act_width = 1920,
	.act_height = 1080,
	.h_front_porch = 88,
	.h_sync = 44,
	.h_back_porch = 148,
	.v_front_porch = 4,
	.v_sync = 5,
	.v_back_porch = 36,

	/* default value for Y */
	.def_y = 0xA0,
	/* default value for Cb */
	.def_cb = 0xB0,
	/* default value for Cr */
	.def_cr = 0xC0,
};

struct mvdu_dev_cfg {
	uintptr_t fx_base;
	uintptr_t gdp_base;
	uintptr_t top_base;
	uint32_t fx_size;
	uint32_t gdp_size;
	uint32_t top_size;
	void (*irq_config_func)(const struct device *dev);
	struct mvdu_dt_settings settings;
};

#define DEV_CFG(_dev_) ((const struct mvdu_dev_cfg *const)(_dev_)->config)
#define DEV_DATA(_dev_) ((struct mvdu_runtime *const)(_dev_)->data)
#define DEV_BASE(_dev_) ((DEV_CFG(_dev_))->base)

#define CLIP(X) ((X) > 255 ? 255 : (X) < 0 ? 0 : X)

#define CRGB2Y(R, G, B) CLIP((19595 * R + 38470 * G + 7471 * B) >> 16)
#define CRGB2Cb(R, G, B)                                                                           \
	CLIP((36962 * (B - CLIP((19595 * R + 38470 * G + 7471 * B) >> 16)) >> 16) + 128)
#define CRGB2Cr(R, G, B)                                                                           \
	CLIP((46727 * (R - CLIP((19595 * R + 38470 * G + 7471 * B) >> 16)) >> 16) + 128)

static uint32_t clamp(uint32_t val, uint32_t lo, uint32_t hi)
{
	return val >= hi ? hi : val <= lo ? lo : val;
}

static int mvdu_setup_memory_interface(const struct device *dev, uint32_t idx,
				       const struct bt_timings *bt)
{
	struct mvdu_runtime *rt = DEV_DATA(dev);
	struct mvdu_mif mif;
	struct vrect crop = rt->stream[idx].crop;
	struct video_format fmt = rt->stream[idx].input_format;
	const unsigned min_height = MIN_HEIGHT;
	const unsigned min_width = MIN_WIDTH;

	mif = rt->stream[idx].mif;

	if (crop.top == 0 && crop.left == 0 && crop.height == 0 && crop.width == 0) {
		crop = fmt.rect;
	} else {
		crop.top = clamp(crop.top, 0, bt->height - min_height);
		crop.left = clamp(crop.left, 0, bt->width - min_width);
		crop.height = clamp(crop.height, min_height, bt->height - crop.top);
		crop.width = clamp(crop.width, min_width, bt->width - crop.left);
	}

	mif.y.height = crop.height;
	mif.y.top = crop.top;
	mif.y.left = crop.left;
	mif.y.width = crop.width;
	mif.y.full_width = fmt.rect.width;
	mif.y.full_height = fmt.rect.height;

	mif.c = mif.y;

	mif.alpha = rt->stream[idx].alpha;

	rt->stream[idx].mif = mif;

	/*
	if (idx == VID)
		mvdu_config_mif_video(rt->fx_base, &mif);
	else if(idx == PIP)
		mvdu_config_mif_pip(rt->fx_base, &mif);
*/

	return 0;
}

static void mvu_update_compose(const struct device *dev, uint32_t idx, const struct bt_timings *bt)
{
	struct mvdu_runtime *rt = DEV_DATA(dev);
	struct vrect compose = rt->stream[idx].compose;

	if (idx == VID) {
		rt->dif.act_hoffset = compose.left;
		rt->dif.act_voffset = compose.top;
	} else if (idx == PIP) {
		rt->dif.pip_hoffset = compose.left;
		rt->dif.pip_voffset = compose.top;
	}
}

static int mvdu_setup_display_interface(const struct device *dev, uint32_t idx,
					const struct bt_timings *bt)
{
	struct mvdu_runtime *rt = DEV_DATA(dev);
	struct mvdu_dif dif;
	struct vrect compose = rt->stream[idx].compose;
	int r, g, b;
	uint32_t fps = (uint32_t)bt->pixelclock / (BT_FRAME_WIDTH(bt) * BT_FRAME_HEIGHT(bt));

	dif = rt->dif;

	if (idx == VID) {
		dif.act_hoffset = compose.left;
		dif.act_voffset = compose.top;
	} else if (idx == PIP) {
		dif.pip_hoffset = compose.left;
		dif.pip_voffset = compose.top;
	}

	dif.act_height = bt->height;
	dif.act_width = bt->width;
	dif.h_back_porch = bt->hbackporch;
	dif.h_front_porch = bt->hfrontporch;
	dif.h_sync = bt->hsync;

	dif.v_back_porch = bt->vbackporch;
	dif.v_front_porch = bt->vfrontporch;
	dif.v_sync = bt->vsync;

	dif.pixelclock = bt->pixelclock;

	dif.hsync_polarity = (bt->polarities & HSYNC_POS_POL) ? HIGH_ACTIVE : LOW_ACTIVE;
	dif.vsync_polarity = (bt->polarities & VSYNC_POS_POL) ? HIGH_ACTIVE : LOW_ACTIVE;
	dif.fsync_polarity = 0;
	dif.pen_polarity = 0;

	r = rt->bgcolor & 0xff;
	g = ((rt->bgcolor >> 8) & 0xff);
	b = ((rt->bgcolor >> 16) & 0xff);

	dif.def_y = CRGB2Y(r, g, b);
	dif.def_cb = CRGB2Cb(r, g, b);
	dif.def_cr = CRGB2Cr(r, g, b);

	LOG_DBG("timing_mode %d, bgcolor 0x%x, fps %d Hz", rt->dif.timing_mode, rt->bgcolor, fps);
	LOG_DBG("width %d, hfrontporch %d, hsync %d, hbackporch %d", bt->width, bt->hfrontporch,
		bt->hsync, bt->hbackporch);
	LOG_DBG("height %d, vfrontporch %d, vsync %d, vbackporch %d", bt->height, bt->vfrontporch,
		bt->vsync, bt->vbackporch);
	LOG_DBG("hsyncpol %c, vsyncpol %c", (bt->polarities & HSYNC_POS_POL) ? '+' : '-',
		(bt->polarities & VSYNC_POS_POL) ? '+' : '-');

	rt->dif = dif;

	return 0;
}

static int mvdu_set_timing(const struct device *dev, uint32_t idx, const struct bt_timings *bt)
{
	struct mvdu_runtime *rt = DEV_DATA(dev);

	rt->timing = *bt;

	return 0;
}

static int mvdu_get_timing(const struct device *dev, uint32_t idx, struct bt_timings *bt)
{
	struct mvdu_runtime *rt = DEV_DATA(dev);

	*bt = rt->timing;

	return 0;
}

static int mvdu_set_input_format(const struct device *dev, uint32_t idx,
				 const struct video_format *fmt)
{
	struct mvdu_runtime *rt = DEV_DATA(dev);

	rt->stream[idx].input_format = *fmt;

	return 0;
}

static int mvdu_get_input_format(const struct device *dev, uint32_t idx, struct video_format *fmt)
{
	struct mvdu_runtime *rt = DEV_DATA(dev);

	*fmt = rt->stream[idx].input_format;

	return 0;
}

static int mvdu_set_input_crop(const struct device *dev, uint32_t idx, const struct vrect *crop)
{
	struct mvdu_runtime *rt = DEV_DATA(dev);

	rt->stream[idx].crop = *crop;

	return 0;
}

static int mvdu_get_input_crop(const struct device *dev, uint32_t idx, struct vrect *crop)
{
	struct mvdu_runtime *rt = DEV_DATA(dev);

	*crop = rt->stream[idx].crop;

	return 0;
}

static int mvdu_set_output_compose(const struct device *dev, uint32_t idx,
				   const struct vrect *compose)
{
	struct mvdu_runtime *rt = DEV_DATA(dev);

	rt->stream[idx].compose = *compose;

	return 0;
}

static int mvdu_get_output_compose(const struct device *dev, uint32_t idx, struct vrect *compose)
{
	struct mvdu_runtime *rt = DEV_DATA(dev);

	*compose = rt->stream[idx].compose;

	return 0;
}

bool mvdu_enabled(const struct device *dev)
{
	struct mvdu_runtime *rt = DEV_DATA(dev);

	return rt->stream[0].enabled || rt->stream[1].enabled || rt->stream[2].enabled;
}

bool mvdu_last_stream(const struct device *dev)
{
	struct mvdu_runtime *rt = DEV_DATA(dev);
	uint32_t i, cnt;

	for (i = 0, cnt = 0; i < MAX_STREAMS; i++) {
		if (rt->stream[i].enabled)
			cnt++;
	}

	return cnt == 1;
}

static int mvdu_set_bgcolor(const struct device *dev, const uint32_t bgcolor)
{
	struct mvdu_runtime *rt = DEV_DATA(dev);

	rt->bgcolor = bgcolor;

	return 0;
}

static int mvdu_get_bgcolor(const struct device *dev, uint32_t *bgcolor)
{
	struct mvdu_runtime *rt = DEV_DATA(dev);

	*bgcolor = rt->bgcolor;

	return 0;
}

static int mvdu_set_alpha(const struct device *dev, uint32_t idx, const uint8_t alpha)
{
	struct mvdu_runtime *rt = DEV_DATA(dev);

	if (idx >= MAX_STREAMS)
		return -1;

	rt->stream[idx].alpha = alpha;

	return 0;
}

static int mvdu_get_alpha(const struct device *dev, uint32_t idx, uint8_t *alpha)
{
	struct mvdu_runtime *rt = DEV_DATA(dev);

	if (idx >= MAX_STREAMS)
		return -1;

	*alpha = rt->stream[idx].alpha;

	return 0;
}

static int mvdu_get_stat(const struct device *dev, uint32_t idx, struct video_stat *stat)
{
	struct mvdu_runtime *rt = DEV_DATA(dev);

	if (idx >= MAX_STREAMS)
		return -1;

	stat->frames = rt->stat.stream[idx].frame;
	stat->underflow = rt->stat.stream[idx].underflow;
	stat->urgent = rt->stat.stream[idx].urgent;

	return 0;
}

static int mvdu_enable_cku(const struct device *dev, uint32_t idx)
{
	struct mvdu_runtime *rt = DEV_DATA(dev);
	struct cku_cfg cku_data = {
		.enable_insular_padding = 0x01,
		.enable_interpol_padding = 0x01,
		.cku_enable = 0x01,
		.cr_key_max = 0x92, // 146
		.cb_key_max = 0x91, // 145
		.y_key_max = 0x90,  // 144
		.cr_key_min = 0x42, // 66
		.cb_key_min = 0x41, // 65
		.y_key_min = 0x40,  // 64
		// .alpha_no_key = 0x90,
		// .alpha_key = 0xE0,
		.alpha_no_key = 0xff,
		.alpha_key = 0x00,
		.alpha_insular_out = 0xE5,
		.alpha_insular_in = 0xD0,
		.alpha_ipol_out = 0xD5,
		.alpha_ipol_in = 0xC0,
	};

	if (idx != PIP)
		return -1;

	cku_data.alpha_no_key = rt->stream[idx].alpha;
	cku_config(rt->fx_base, &cku_data);
	cku_enable(rt->fx_base, 1);

	return 0;
}

static void mvdu_init_top(const struct device *dev)
{
	struct mvdu_runtime *rt = DEV_DATA(dev);

	mvdu_write(rt->top.omux_ctrl, rt->top_base + MVDU_OMUX_CTRL_REG);
}

static void mvdu_init_mif(const struct device *dev, uint32_t idx)
{
	struct mvdu_runtime *rt = DEV_DATA(dev);

	rt->stream[idx].mif.color_mode_422 = true;
	rt->stream[idx].mif.little_endian = true;
	rt->stream[idx].mif.small_packet_en = false;
}

static int mvdu_reset_stat(const struct device *dev)
{
	struct mvdu_runtime *rt = DEV_DATA(dev);

	memset(&rt->stat, 0x00, sizeof(struct mvdu_stat));

	return 0;
}

static int mvdu_reset_stat_chan(const struct device *dev, uint32_t idx)
{
	struct mvdu_runtime *rt = DEV_DATA(dev);

	if (idx >= MAX_STREAMS)
		return -1;

	memset(&rt->stat.stream[idx], 0x00, sizeof(struct mvdu_stream_stat));

	return 0;
}

static void mvdu_video_base_update(const struct device *dev, uintptr_t y_base, uintptr_t c_base)
{
	struct mvdu_runtime *rt = DEV_DATA(dev);

	mvdu_update_display_start_pos(rt->fx_base, &rt->dif);
	mvdu_mi_write(y_base, rt->fx_base + MI_V1_Y_BA_REG);
	mvdu_mi_write(c_base, rt->fx_base + MI_V1_C_BA_REG);
	mvdu_config_mif_video(rt->fx_base, &rt->stream[VID].mif);
	mvdu_dif_frame_start(rt->fx_base, false);
}

static void mvdu_pip_base_update(const struct device *dev, uintptr_t y_base, uintptr_t c_base)
{
	struct mvdu_runtime *rt = DEV_DATA(dev);

	mvdu_update_display_start_pos(rt->fx_base, &rt->dif);
	mvdu_mi_write(y_base, rt->fx_base + MI_PIP_IN_Y_BA_REG);
	mvdu_mi_write(c_base, rt->fx_base + MI_PIP_IN_C_BA_REG);
	mvdu_config_mif_pip(rt->fx_base, &rt->stream[PIP].mif);
}

static void mvdu_osd_base_update(const struct device *dev, uintptr_t rgb_base,
				 uintptr_t zone_base_phys, uintptr_t zone_base_virt)
{
	struct mvdu_runtime *rt = DEV_DATA(dev);

	LOG_DBG("%s", __func__);

	mvdu_write(DIF_OSD1_UFLW_CLR_MASK, rt->fx_base + MVDU_CTRL_BASE + MVDU_ICR_REG);

	mvdu_config_zones(&rt->stream[OSD].mif, zone_base_virt, rgb_base);
	mvdu_config_mif_osd(rt->gdp_base, zone_base_phys);
}

int mvdu_sched_next(const struct device *dev, uint32_t idx, struct video_buf *buf)
{
	struct mvdu_runtime *rt = DEV_DATA(dev);
	uintptr_t phys_base[2] = {0, 0};
	uintptr_t virt_base[2] = {0, 0};

	if (buf == NULL)
		return -1;

	if (idx >= MAX_STREAMS)
		return -2;

	if (!buf->num_planes)
		return -3;

	if (buf->num_planes >= 1)
		phys_base[0] = buf->planes[0].phys;

	if (buf->num_planes >= 2) {
		phys_base[1] = buf->planes[1].phys;
		virt_base[1] = buf->planes[1].virt;
	}

	LOG_DBG("%s, idx %d, seq %u, planes %d, y_base %lx, c_base %lx", __func__, idx,
		buf->sequence, buf->num_planes, phys_base[0], phys_base[1]);

	if (idx == VID)
		mvdu_video_base_update(dev, phys_base[0], phys_base[1]);
	else if (idx == PIP)
		mvdu_pip_base_update(dev, phys_base[0], phys_base[1]);
	else if (idx == OSD)
		mvdu_osd_base_update(dev, phys_base[0], phys_base[1], virt_base[1]);

	rt->stat.stream[idx].frame++;

	return 0;
}

static int mvdu_start_stream(const struct device *dev, uint32_t idx, struct video_buf *vbuf)
{
	struct mvdu_runtime *rt = DEV_DATA(dev);
	int ret;

	if (idx >= MAX_STREAMS)
		return -1;

	if (!rt->stream[idx].input_format.fourcc)
		return -2;

	mvdu_reset_stat_chan(dev, idx);
	mvdu_init_top(dev);
	mvdu_init_mif(dev, idx);

	if (!mvdu_enabled(dev)) {
		ret = mvdu_setup_display_interface(dev, idx, &rt->timing);
		if (ret) {
			LOG_ERR("can't configure timing");
			return ret;
		}
	} else {
		mvu_update_compose(dev, idx, &rt->timing);
	}

	ret = mvdu_setup_memory_interface(dev, idx, &rt->timing);
	if (ret) {
		LOG_ERR("can't configure timing");
		return ret;
	}

	if (!mvdu_enabled(dev)) {
		mvdu_config_display(rt->fx_base, &rt->dif);
		mvdu_start_vtg(rt->fx_base);
		mvdu_config_display(rt->fx_base, &rt->dif);
	}

	ret = mvdu_sched_next(dev, idx, vbuf);
	if (ret) {
		LOG_ERR("can't schedule buffer");
		return ret;
	}

	if (idx == VID)
		mvdu_enable_video(rt->fx_base);
	else if (idx == PIP) {
		mvdu_enable_cku(dev, idx);
		mvdu_enable_pip(rt->fx_base);
	} else if (idx == OSD)
		mvdu_enable_osd(rt->fx_base, rt->gdp_base);

	if (!mvdu_enabled(dev))
		mvdu_enable_disp_finish_irq(rt->fx_base);

	rt->stream[idx].enabled = true;

	return 0;
}

static int mvdu_stop_stream(const struct device *dev, uint32_t idx)
{
	struct mvdu_runtime *rt = DEV_DATA(dev);
	bool last = mvdu_last_stream(dev);

	if (idx >= MAX_STREAMS)
		return -1;

	rt->stream[idx].enabled = false;

	if (idx == VID)
		mvdu_disable_video(rt->fx_base);
	else if (idx == PIP)
		mvdu_disable_pip(rt->fx_base);
	else if (idx == OSD)
		mvdu_disable_osd(rt->fx_base, rt->gdp_base);

	if (last) {
		mvdu_disable_disp_finish_irq(rt->fx_base);
		mvdu_stop_vtg(rt->fx_base);
	}

	return 0;
}

static int mvdu_exit(const struct device *dev)
{
	return 0;
}

static int mvdu_reset(const struct device *dev)
{
	return 0;
}

static int mvdu_probe(const struct device *dev)
{
	const struct mvdu_dev_cfg *cfg = DEV_CFG(dev);
	struct mvdu_runtime *rt = DEV_DATA(dev);
	uint32_t val = 0;

#if defined(CONFIG_MMU)
	device_map((mm_reg_t *)&rt->top_base, cfg->top_base, cfg->top_size, K_MEM_CACHE_NONE);
	device_map((mm_reg_t *)&rt->fx_base, cfg->fx_base, cfg->fx_size, K_MEM_CACHE_NONE);
	device_map((mm_reg_t *)&rt->gdp_base, cfg->gdp_base, cfg->gdp_size, K_MEM_CACHE_NONE);
#else
	rt->top_base = cfg->top_base;
	rt->fx_base = cfg->fx_base;
	rt->gdp_base = cfg->gdp_base;
#endif

	val = mvdu_read(rt->top_base + MVDU_OMUX_CTRL_REG);
	LOG_DBG("%s MVDU_OMUX_CTRL_REG 0x%x", __func__, val);
	val &= ~0x37;
	if (cfg->settings.mipi_enable)
		val |= 0x20;
	if (cfg->settings.tv6xy_enable)
		val |= 0x10;
	if (cfg->settings.cb_cr_swap_enable)
		val |= 0x04;
	if (cfg->settings.luma_chroma_swap_enable)
		val |= 0x02;
	if (cfg->settings.video_out_bypass_enable)
		val |= 0x01;
	LOG_DBG("%s MVDU_OMUX_CTRL_REG 0x%x", __func__, val);
	rt->top.omux_ctrl = val;

	memcpy(&rt->dif, &def_display_info, sizeof(struct mvdu_dif));
	rt->dif.timing_mode = cfg->settings.timing_mode;
	rt->timing = default_timing;

	cfg->irq_config_func(dev);

	rt->bgcolor = 0x808080;

	return 0;
}

static int mvdu_dump(const struct device *dev)
{
	struct mvdu_runtime *rt = DEV_DATA(dev);
	uintptr_t base;

	base = rt->fx_base;

#define REG_DUMP(b, r) printk("%08x = %08x /* %s */\n", r##_REG, mvdu_read(b + r##_REG), #r)
#define REG_DIF_DUMP(b, r)                                                                         \
	printk("%08x = %08x /* %s */\n", MVDU_DIF_BASE + r##_REG,                                  \
	       mvdu_read(b + MVDU_DIF_BASE + r##_REG), #r)
#define REG_MI_DUMP(b, r)                                                                          \
	printk("%08x = %08x /* %s */\n", MVDU_MI_BASE + r##_REG,                                   \
	       mvdu_read(b + MVDU_MI_BASE + r##_REG), #r)
#define REG_OSD_DUMP(b, r)                                                                         \
	printk("%08x = %08x /* %s */\n", MVDU_OSD_BASE + r##_REG,                                  \
	       mvdu_read(b + MVDU_OSD_BASE + r##_REG), #r)

	REG_DIF_DUMP(base, DIF_STATUS);
	REG_DIF_DUMP(base, DIF_CONFIG);
	REG_DIF_DUMP(base, DIF_LINE_CNT_THRES);
	REG_DIF_DUMP(base, DIF_DEFAULT_COL);
	REG_DIF_DUMP(base, DIF_FRAM_START);
	REG_DIF_DUMP(base, DIF_DISP_FINISH_POS);
	REG_DIF_DUMP(base, DIF_SAV_START);
	REG_DIF_DUMP(base, DIF_PIC_START);
	REG_DIF_DUMP(base, DIF_PIC_STOP);
	REG_DIF_DUMP(base, DIF_CLIPPING);
	REG_DIF_DUMP(base, DIF_PATH_ENABLE);
	REG_DIF_DUMP(base, DIF_POS_MAX);
	REG_DIF_DUMP(base, DIF_LINE_MAX);
	REG_DIF_DUMP(base, DIF_EAV_F_START);
	REG_DIF_DUMP(base, DIF_EAV_F_STOP);
	REG_DIF_DUMP(base, DIF_HSYNC_START);
	REG_DIF_DUMP(base, DIF_HSYNC_STOP);
	REG_DIF_DUMP(base, DIF_VSYNC_START);
	REG_DIF_DUMP(base, DIF_VSYNC_STOP);
	REG_DIF_DUMP(base, DIF_FSYNC_START);
	REG_DIF_DUMP(base, DIF_FSYNC_STOP);
	REG_DIF_DUMP(base, DIF_PIP_START);
	REG_DIF_DUMP(base, DIF_ACTIVE_START);
	REG_DIF_DUMP(base, DIF_INIT);

	REG_MI_DUMP(base, MI_V1_Y_NR_OF_ROWS);
	REG_MI_DUMP(base, MI_V1_C_NR_OF_ROWS);
	REG_MI_DUMP(base, MI_V1_Y_LINE_SIZE);
	REG_MI_DUMP(base, MI_V1_C_LINE_SIZE);
	REG_MI_DUMP(base, MI_V1_Y_FULL_LINE_SIZE);
	REG_MI_DUMP(base, MI_V1_C_FULL_LINE_SIZE);
	REG_MI_DUMP(base, MI_V1_Y_START_POS);
	REG_MI_DUMP(base, MI_V1_C_START_POS);
	REG_MI_DUMP(base, MI_PIP_IN_Y_NR_OF_ROWS);
	REG_MI_DUMP(base, MI_PIP_IN_C_NR_OF_ROWS);
	REG_MI_DUMP(base, MI_PIP_IN_Y_LINE_SIZE);
	REG_MI_DUMP(base, MI_PIP_IN_C_LINE_SIZE);
	REG_MI_DUMP(base, MI_PIP_IN_Y_FULL_LINE_SIZE);
	REG_MI_DUMP(base, MI_PIP_IN_C_FULL_LINE_SIZE);
	REG_MI_DUMP(base, MI_PIP_IN_Y_START_POS);
	REG_MI_DUMP(base, MI_PIP_IN_C_START_POS);
	REG_MI_DUMP(base, MI_V1_Y_BA);
	REG_MI_DUMP(base, MI_V1_C_BA);
	REG_MI_DUMP(base, MI_V1_CONFIG);
	REG_MI_DUMP(base, MI_PIP_IN_Y_BA);
	REG_MI_DUMP(base, MI_PIP_IN_C_BA);
	REG_MI_DUMP(base, MI_PIP_IN_CONFIG);

	REG_OSD_DUMP(base, OSD_CONFIG);

	REG_DUMP(base, DATAPATH_STAT);
	REG_DUMP(base, DATAPATH_CONFIG);
	REG_DUMP(base, MVDU_MSK);
	REG_DUMP(base, MVDU_MIS);
	REG_DUMP(base, MVDU_RIS);

	return 0;
}

static int mvdu_register_callback(const struct device *dev, uint32_t idx,
				  int (*irq_handler)(uint32_t idx, void *irq_ctx), void *irq_ctx)
{
	struct mvdu_runtime *rt = DEV_DATA(dev);
	int ret = 0;

	if (idx >= MAX_STREAMS)
		return -2;

	rt->stream[idx].irq_handler = irq_handler;
	rt->stream[idx].irq_ctx = irq_ctx;

	return ret;
}

static void mvdu_fx_isr(void *arg)
{
	const struct device *dev = (const struct device *)arg;
	struct mvdu_runtime *rt = DEV_DATA(dev);
	struct mvdu_stream_data *s;
	uint32_t mis, i;

	mis = mvdu_read(rt->fx_base + MVDU_RIS_REG);

	if (mis & DIF_DISP_FINISH_MSK_MASK) {
		s = &rt->stream[0];
		for (i = 0; i < MAX_STREAMS; i++, s++) {
			if (s->enabled && s->irq_handler)
				s->irq_handler(i, s->irq_ctx);
		}
		rt->stat.disp_finish++;
	}
	if (mis & DIF_LINE_TRESHOLD_MSK_MASK)
		rt->stat.line_treshold++;
	if (mis & DIF_VID1_UFLW_MSK_MASK)
		rt->stat.stream[VID].underflow++;
	if (mis & DIF_PIP_UFLW_MSK_MASK)
		rt->stat.stream[PIP].underflow++;
	if (mis & DIF_OSD1_UFLW_MSK_MASK)
		rt->stat.stream[OSD].underflow++;
	if (mis & VID1_MEM_URGENT_MSK_MASK)
		rt->stat.stream[VID].urgent++;
	if (mis & PIPIN_MEM_URGENT_MSK_MASK)
		rt->stat.stream[PIP].urgent++;
	if (mis & STOP_VTG_FINISH_MSK_MASK)
		rt->stat.stop_vtg_finish++;
	//	if (mis & PIPOUT_FINISH_MSK_MASK)
	//		printk("PIPOUT_FINISH_MSK_MASK\n");

	mvdu_write(mis, rt->fx_base + MVDU_ICR_REG);
}

static void mvdu_gdp_isr(void *arg)
{
	const struct device *dev = (const struct device *)arg;
	struct mvdu_runtime *rt = DEV_DATA(dev);
	uint32_t mis;

	mis = mvdu_read(rt->gdp_base + GDP_RIS_REG);

	if (mis & BIT(6))
		rt->stat.stream[OSD].urgent++;

	mvdu_write(mis, rt->gdp_base + GDP_ICR_REG);
}

uint32_t mvdu_read(uintptr_t base)
{
	return sys_read32(base);
}

void mvdu_write(uint32_t val, uintptr_t base)
{
	sys_write32(val, base);
}

void mvdu_get_dt_cfg(const struct device *dev, struct mvdu_dt_settings *dest)
{
	const struct mvdu_dev_cfg *src = DEV_CFG(dev);

	*dest = src->settings;
}

static const struct video_driver_api mvdu_driver_api = {
	.dump = mvdu_dump,
	.exit = mvdu_exit,
	.get_alpha = mvdu_get_alpha,
	.get_bgcolor = mvdu_get_bgcolor,
	.get_input_crop = mvdu_get_input_crop,
	.get_input_format = mvdu_get_input_format,
	.get_output_compose = mvdu_get_output_compose,
	.get_stat = mvdu_get_stat,
	.get_timing = mvdu_get_timing,
	.register_callback = mvdu_register_callback,
	.reset = mvdu_reset,
	.reset_stat = mvdu_reset_stat,
	.set_alpha = mvdu_set_alpha,
	.set_bgcolor = mvdu_set_bgcolor,
	.set_input_crop = mvdu_set_input_crop,
	.set_input_format = mvdu_set_input_format,
	.set_output_compose = mvdu_set_output_compose,
	.set_timing = mvdu_set_timing,
	.start_stream = mvdu_start_stream,
	.stop_stream = mvdu_stop_stream,
};

#define MVDU_INIT(n)                                                                               \
	static void mvdu_config_func_##n(const struct device *dev);                                \
	static struct mvdu_runtime mvdu_runtime_##n;                                               \
                                                                                                   \
	static const struct mvdu_dev_cfg mvdu_cfg_##n = {                                          \
		.fx_base = DT_INST_REG_ADDR_BY_IDX(n, 0),                                          \
		.gdp_base = DT_INST_REG_ADDR_BY_IDX(n, 1),                                         \
		.top_base = DT_INST_REG_ADDR_BY_IDX(n, 2),                                         \
		.fx_size = DT_INST_REG_SIZE_BY_IDX(n, 0),                                          \
		.gdp_size = DT_INST_REG_SIZE_BY_IDX(n, 1),                                         \
		.top_size = DT_INST_REG_SIZE_BY_IDX(n, 2),                                         \
		.irq_config_func = mvdu_config_func_##n,                                           \
		.settings.timing_mode = DT_INST_PROP_OR(n, timing_mode, TM_YC_EXSYNC_1REPL),       \
		.settings.mipi_enable = DT_INST_PROP_OR(n, mipi_enable, 0),                        \
		.settings.tv6xy_enable = DT_INST_PROP_OR(n, tv6xy_enable, 0),                      \
		.settings.cb_cr_swap_enable = DT_INST_PROP_OR(n, cb_cr_swap_enable, 0),            \
		.settings.luma_chroma_swap_enable =                                                \
			DT_INST_PROP_OR(n, luma_chroma_swap_enable, 0),                            \
		.settings.video_out_bypass_enable =                                                \
			DT_INST_PROP_OR(n, video_out_bypass_enable, 0),                            \
	};                                                                                         \
                                                                                                   \
	DEVICE_DT_INST_DEFINE(n, &mvdu_probe, NULL, &mvdu_runtime_##n, &mvdu_cfg_##n, POST_KERNEL, \
			      CONFIG_KERNEL_INIT_PRIORITY_DEVICE, &mvdu_driver_api);               \
                                                                                                   \
	static void mvdu_config_func_##n(const struct device *dev)                                 \
	{                                                                                          \
		IRQ_CONNECT(DT_INST_IRQ_BY_IDX(n, 0, irq), DT_INST_IRQ_BY_IDX(n, 0, priority),     \
			    mvdu_fx_isr, DEVICE_DT_INST_GET(n), 0);                                \
		irq_enable(DT_INST_IRQ_BY_IDX(n, 0, irq));                                         \
                                                                                                   \
		IRQ_CONNECT(DT_INST_IRQ_BY_IDX(n, 1, irq), DT_INST_IRQ_BY_IDX(n, 1, priority),     \
			    mvdu_gdp_isr, DEVICE_DT_INST_GET(n), 0);                               \
		irq_enable(DT_INST_IRQ_BY_IDX(n, 1, irq));                                         \
	}

DT_INST_FOREACH_STATUS_OKAY(MVDU_INIT)
