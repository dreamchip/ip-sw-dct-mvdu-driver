/*
 * Copyright (c) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: Apache-2.0
 */

/**
 * @file
 * Public APIs for video drivers
 */

#ifndef __ZEPHYR_INCLUDE_DRIVERS_VIDEO_H__
#define __ZEPHYR_INCLUDE_DRIVERS_VIDEO_H__

/**
 * @brief Video Interface
 * @defgroup video_interface Video Interface
 * @ingroup io_interfaces
 * @{
 */

#include <zephyr/device.h>
#include <errno.h>
#include <zephyr/types.h>

#ifdef __cplusplus
extern "C" {
#endif

#define VIDEO_MAX_PLANES 3
#define HSYNC_POS_POL 1
#define VSYNC_POS_POL 2

#define BUS_FMT_UYVY8_1X16 0
#define BUS_FMT_YUYV8_1X16 1
#define BUS_FMT_RGB888_1X24 2

#define FOURCC(a, b, c, d)                                                                         \
	((uint32_t)(a) | ((uint32_t)(b) << 8) | ((uint32_t)(c) << 16) | ((uint32_t)(d) << 24))

#define PIX_FMT_NV61M FOURCC('N', 'M', '6', '1')  /* 16  Y/CrCb 4:2:2  */
#define PIX_FMT_RGBA32 FOURCC('A', 'B', '2', '4') /* 32  RGBA-8-8-8-8  */

#define INIT_BT_TIMINGS(_width, _height, _polarities, _pixelclock, _hfrontporch, _hsync,           \
			_hbackporch, _vfrontporch, _vsync, _vbackporch)                            \
	{                                                                                          \
		.width = _width, .height = _height, .polarities = _polarities,                     \
		.pixelclock = _pixelclock, .hfrontporch = _hfrontporch, .hsync = _hsync,           \
		.hbackporch = _hbackporch, .vfrontporch = _vfrontporch, .vsync = _vsync,           \
		.vbackporch = _vbackporch,                                                         \
	}

#define DV_BT_CEA_720X480P59 INIT_BT_TIMINGS(720, 480, 0, 27000000, 16, 62, 60, 9, 6, 30)

#define DV_BT_CEA_1280X720P24                                                                      \
	INIT_BT_TIMINGS(1280, 720, HSYNC_POS_POL | VSYNC_POS_POL, 59400000, 1760, 40, 220, 5, 5, 20)

#define DV_BT_CEA_1920X1080P24                                                                     \
	INIT_BT_TIMINGS(1920, 1080, HSYNC_POS_POL | VSYNC_POS_POL, 74250000, 638, 44, 148, 4, 5, 36)

#define DV_BT_CEA_1920X1080P25                                                                     \
	INIT_BT_TIMINGS(1920, 1080, HSYNC_POS_POL | VSYNC_POS_POL, 74250000, 528, 44, 148, 4, 5, 36)

#define DV_BT_CEA_1920X1080P30                                                                     \
	INIT_BT_TIMINGS(1920, 1080, HSYNC_POS_POL | VSYNC_POS_POL, 74250000, 88, 44, 148, 4, 5, 36)

#define DV_BT_CEA_1920X1080P50                                                                     \
	INIT_BT_TIMINGS(1920, 1080, HSYNC_POS_POL | VSYNC_POS_POL, 148500000, 528, 44, 148, 4, 5,  \
			36)

#define DV_BT_CEA_1920X1080P60                                                                     \
	INIT_BT_TIMINGS(1920, 1080, HSYNC_POS_POL | VSYNC_POS_POL, 148500000, 88, 44, 148, 4, 5, 36)

#define BT_BLANKING_WIDTH(bt) ((bt)->hfrontporch + (bt)->hsync + (bt)->hbackporch)
#define BT_FRAME_WIDTH(bt) ((bt)->width + BT_BLANKING_WIDTH(bt))
#define BT_BLANKING_HEIGHT(bt) ((bt)->vfrontporch + (bt)->vsync + (bt)->vbackporch)
#define BT_FRAME_HEIGHT(bt) ((bt)->height + BT_BLANKING_HEIGHT(bt))

struct bt_timings {
	uint32_t width;
	uint32_t height;
	uint32_t polarities;
	uint64_t pixelclock;
	uint32_t hfrontporch;
	uint32_t hsync;
	uint32_t hbackporch;
	uint32_t vfrontporch;
	uint32_t vsync;
	uint32_t vbackporch;
};

struct plane_pix_format {
	uint32_t sizeimage;
	uint32_t bytesperline;
};

struct pix_format_mplane {
	uint32_t width;
	uint32_t height;
	uint32_t pixelformat;
	uint32_t colorspace;
	uint32_t num_planes;
	struct plane_pix_format formats[VIDEO_MAX_PLANES];
};

struct video_plane {
	uintptr_t phys;
	uintptr_t virt;
	uint32_t size;
};

struct vrect {
	int32_t left;
	int32_t top;
	uint32_t width;
	uint32_t height;
};

enum vbuf_state { VBUF_DISPLAY, VBUF_DONE };

struct video_buf {
	uintptr_t reserved;
	uint32_t num_planes;
	uint32_t sequence;
	struct video_plane planes[VIDEO_MAX_PLANES];
	enum vbuf_state state;
};

struct video_stat {
	uint32_t frames;
	uint32_t underflow;
	uint32_t urgent;
};

struct video_format {
	uint32_t fourcc;
	struct vrect rect;
};

/**
 * @typedef video_api_init
 * @brief Callback API to initialize the video device
 */
typedef int (*video_api_init)(const struct device *dev);

/**
 * @typedef video_api_exit
 * @brief Callback API to release the video device
 */
typedef int (*video_api_exit)(const struct device *dev);

/**
 * @typedef video_api_set_timing
 * @brief Callback API to set timing of the video device
 */
typedef int (*video_api_set_timing)(const struct device *dev, uint32_t stream,
				    const struct bt_timings *bt);

/**
 * @typedef video_api_get_timing
 * @brief Callback API to get timing of the video device
 */
typedef int (*video_api_get_timing)(const struct device *dev, uint32_t stream,
				    struct bt_timings *bt);

/**
 * @typedef video_api_start_stream
 * @brief Callback API to start streaming of the video device
 */
typedef int (*video_api_start_stream)(const struct device *dev, uint32_t stream,
				      struct video_buf *buf);

/**
 * @typedef video_api_stop_stream
 * @brief Callback API to stop streaming of the video device
 */
typedef int (*video_api_stop_stream)(const struct device *dev, uint32_t stream);

/**
 * @typedef video_api_configure
 * @brief Callback API to configure the video device
 */
typedef int (*video_api_configure)(const struct device *dev, const void *config);

/**
 * @typedef video_api_reset
 * @brief Callback API to reset the video device
 */
typedef int (*video_api_reset)(const struct device *dev);

/**
 * @typedef video_api_dump
 * @brief Callback API to dump internal register of the video device
 */
typedef int (*video_api_dump)(const struct device *dev);

/**
 * @typedef video_api_config_changed
 * @brief Callback API to check if configuration of the video device  has changed
 */
typedef bool (*video_api_config_changed)(const struct device *dev);

/**
 * @typedef video_api_register_callback
 * @brief Callback API to install application IRQ callback - handle video frames
 */
typedef int (*video_api_register_callback)(const struct device *dev, uint32_t stream,
					   int (*irq_handler)(uint32_t idx, void *irq_ctx),
					   void *irq_ctx);

/**
 * @typedef video_api_set_bgcolor
 * @brief Callback API to set bgcolor of the video device
 */
typedef int (*video_api_set_bgcolor)(const struct device *dev, const uint32_t bgcolor);

/**
 * @typedef video_api_get_bgcolor
 * @brief Callback API to get bgcolor of the video device
 */
typedef int (*video_api_get_bgcolor)(const struct device *dev, uint32_t *bgcolor);

/**
 * @typedef video_api_get_stat
 * @brief Callback API to get statistics of the video device
 */
typedef int (*video_api_get_stat)(const struct device *dev, uint32_t stream,
				  struct video_stat *stat);

/**
 * @typedef video_api_reset_stat
 * @brief Callback API to reset statistics of the video device
 */
typedef int (*video_api_reset_stat)(const struct device *dev);

/**
 * @typedef video_api_set_input_format
 * @brief Callback API to set format of the video stream
 */
typedef int (*video_api_set_input_format)(const struct device *dev, uint32_t stream,
					  const struct video_format *fmt);

/**
 * @typedef video_api_get_input_format
 * @brief Callback API to get format of the video stream
 */
typedef int (*video_api_get_input_format)(const struct device *dev, uint32_t stream,
					  struct video_format *fmt);

/**
 * @typedef video_api_set_input_crop
 * @brief Callback API to set input crop of the video stream
 */
typedef int (*video_api_set_input_crop)(const struct device *dev, uint32_t stream,
					const struct vrect *rect);

/**
 * @typedef video_api_get_input_crop
 * @brief Callback API to get input crop of the video stream
 */
typedef int (*video_api_get_input_crop)(const struct device *dev, uint32_t stream,
					struct vrect *rect);

/**
 * @typedef video_api_set_output_compose
 * @brief Callback API to set output position of the video stream
 */
typedef int (*video_api_set_output_compose)(const struct device *dev, uint32_t stream,
					    const struct vrect *rect);

/**
 * @typedef video_api_get_output_compose
 * @brief Callback API to get output position of the video stream
 */
typedef int (*video_api_get_output_compose)(const struct device *dev, uint32_t stream,
					    struct vrect *rect);

/**
 * @typedef video_api_set_alpha
 * @brief Callback API to set alpha of the video device
 */
typedef int (*video_api_set_alpha)(const struct device *dev, uint32_t stream, const uint8_t alpha);

/**
 * @typedef video_api_get_alpha
 * @brief Callback API to get alpha of the video device
 */
typedef int (*video_api_get_alpha)(const struct device *dev, uint32_t stream, uint8_t *alpha);

__subsystem struct video_driver_api {
	video_api_init init;
	video_api_exit exit;
	video_api_set_timing set_timing;
	video_api_get_timing get_timing;
	video_api_start_stream start_stream;
	video_api_stop_stream stop_stream;
	video_api_configure configure;
	video_api_reset reset;
	video_api_dump dump;
	video_api_config_changed config_changed;
	video_api_register_callback register_callback;
	video_api_set_bgcolor set_bgcolor;
	video_api_get_bgcolor get_bgcolor;
	video_api_get_stat get_stat;
	video_api_reset_stat reset_stat;
	video_api_set_input_format set_input_format;
	video_api_get_input_format get_input_format;
	video_api_set_input_crop set_input_crop;
	video_api_get_input_crop get_input_crop;
	video_api_set_output_compose set_output_compose;
	video_api_get_output_compose get_output_compose;
	video_api_set_alpha set_alpha;
	video_api_get_alpha get_alpha;
};

static inline int video_init(const struct device *dev)
{
	const struct video_driver_api *api = (const struct video_driver_api *)dev->api;

	return api->init ? api->init(dev) : -ENOTSUP;
}

static inline int video_exit(const struct device *dev)
{
	const struct video_driver_api *api = (const struct video_driver_api *)dev->api;

	return api->exit ? api->exit(dev) : -ENOTSUP;
}

static inline int video_set_timing(const struct device *dev, uint32_t stream,
				   const struct bt_timings *bt)
{
	const struct video_driver_api *api = (const struct video_driver_api *)dev->api;

	return api->set_timing ? api->set_timing(dev, stream, bt) : -ENOTSUP;
}

static inline int video_get_timing(const struct device *dev, uint32_t stream, struct bt_timings *bt)
{
	const struct video_driver_api *api = (const struct video_driver_api *)dev->api;

	return api->get_timing ? api->get_timing(dev, stream, bt) : -ENOTSUP;
}

static inline int video_start_stream(const struct device *dev, uint32_t stream,
				     struct video_buf *vbuf)
{
	const struct video_driver_api *api = (const struct video_driver_api *)dev->api;

	return api->start_stream ? api->start_stream(dev, stream, vbuf) : -ENOTSUP;
}

static inline int video_stop_stream(const struct device *dev, uint32_t stream)
{
	const struct video_driver_api *api = (const struct video_driver_api *)dev->api;

	return api->stop_stream ? api->stop_stream(dev, stream) : -ENOTSUP;
}

static inline int video_configure(const struct device *dev, const void *config)
{
	const struct video_driver_api *api = (const struct video_driver_api *)dev->api;

	return api->configure ? api->configure(dev, config) : -ENOTSUP;
}

static inline int video_reset(const struct device *dev)
{
	const struct video_driver_api *api = (const struct video_driver_api *)dev->api;

	return api->reset ? api->reset(dev) : -ENOTSUP;
}

static inline int video_dump(const struct device *dev)
{
	const struct video_driver_api *api = (const struct video_driver_api *)dev->api;

	return api->dump ? api->dump(dev) : -ENOTSUP;
}

static inline bool video_config_changed(const struct device *dev)
{
	const struct video_driver_api *api = (const struct video_driver_api *)dev->api;

	return api->config_changed ? api->config_changed(dev) : true;
}

static inline int video_register_callback(const struct device *dev, uint32_t stream,
					  int (*irq_handler)(uint32_t idx, void *irq_ctx),
					  void *irq_ctx)
{
	const struct video_driver_api *api = (const struct video_driver_api *)dev->api;

	return api->register_callback ? api->register_callback(dev, stream, irq_handler, irq_ctx) :
					-ENOTSUP;
}

static inline int video_set_bgcolor(const struct device *dev, const uint32_t bgcolor)
{
	const struct video_driver_api *api = (const struct video_driver_api *)dev->api;

	return api->set_bgcolor ? api->set_bgcolor(dev, bgcolor) : -ENOTSUP;
}

static inline int video_get_bgcolor(const struct device *dev, uint32_t *bgcolor)
{
	const struct video_driver_api *api = (const struct video_driver_api *)dev->api;

	return api->get_bgcolor ? api->get_bgcolor(dev, bgcolor) : -ENOTSUP;
}

static inline int video_reset_stat(const struct device *dev)
{
	const struct video_driver_api *api = (const struct video_driver_api *)dev->api;

	return api->reset_stat ? api->reset_stat(dev) : -ENOTSUP;
}

static inline int video_get_stat(const struct device *dev, uint32_t stream, struct video_stat *stat)
{
	const struct video_driver_api *api = (const struct video_driver_api *)dev->api;

	return api->get_stat ? api->get_stat(dev, stream, stat) : -ENOTSUP;
}

static inline int video_set_input_format(const struct device *dev, uint32_t stream,
					 const struct video_format *fmt)
{
	const struct video_driver_api *api = (const struct video_driver_api *)dev->api;

	return api->set_input_format ? api->set_input_format(dev, stream, fmt) : -ENOTSUP;
}

static inline int video_get_input_format(const struct device *dev, uint32_t stream,
					 struct video_format *fmt)
{
	const struct video_driver_api *api = (const struct video_driver_api *)dev->api;

	return api->get_input_format ? api->get_input_format(dev, stream, fmt) : -ENOTSUP;
}

static inline int video_set_input_crop(const struct device *dev, uint32_t stream,
				       const struct vrect *rect)
{
	const struct video_driver_api *api = (const struct video_driver_api *)dev->api;

	return api->set_input_crop ? api->set_input_crop(dev, stream, rect) : -ENOTSUP;
}

static inline int video_get_input_crop(const struct device *dev, uint32_t stream,
				       struct vrect *rect)
{
	const struct video_driver_api *api = (const struct video_driver_api *)dev->api;

	return api->get_input_crop ? api->get_input_crop(dev, stream, rect) : -ENOTSUP;
}

static inline int video_set_output_compose(const struct device *dev, uint32_t stream,
					   const struct vrect *rect)
{
	const struct video_driver_api *api = (const struct video_driver_api *)dev->api;

	return api->set_output_compose ? api->set_output_compose(dev, stream, rect) : -ENOTSUP;
}

static inline int video_get_output_compose(const struct device *dev, uint32_t stream,
					   struct vrect *rect)
{
	const struct video_driver_api *api = (const struct video_driver_api *)dev->api;

	return api->get_output_compose ? api->get_output_compose(dev, stream, rect) : -ENOTSUP;
}

static inline int video_set_alpha(const struct device *dev, uint32_t stream, const uint8_t alpha)
{
	const struct video_driver_api *api = (const struct video_driver_api *)dev->api;

	return api->set_alpha ? api->set_alpha(dev, stream, alpha) : -ENOTSUP;
}

static inline int video_get_alpha(const struct device *dev, uint32_t stream, uint8_t *alpha)
{
	const struct video_driver_api *api = (const struct video_driver_api *)dev->api;

	return api->get_alpha ? api->get_alpha(dev, stream, alpha) : -ENOTSUP;
}

#ifdef __cplusplus
}
#endif

/**
 *
 * @}
 */

#endif /* __ZEPHYR_INCLUDE_DRIVERS_VIDEO_H__ */
