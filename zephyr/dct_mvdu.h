/*
 * Copyright (c) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: Apache-2.0
 */

/**
 * @file
 *
 * The video driver provides access to the DCT Multi-Stream Video Display Unit.
 */

#ifndef __DCT_MVDU_H__
#define __DCT_MVDU_H__

#include <zephyr/device.h>

#include <video.h>

struct mvdu_config {
	uint32_t data_rate;
};

enum {
	VID = 0, // video stream
	PIP = 1, // pictur-in-picture
	OSD = 2, // on-screen-display
	MAX_STREAMS,
};

struct mvdu_stream_stat {
	uint32_t urgent;
	uint32_t underflow;
	uint32_t frame;
};

struct mvdu_stat {
	uint32_t disp_finish;
	uint32_t line_treshold;
	uint32_t stop_vtg_finish;
	struct mvdu_stream_stat stream[MAX_STREAMS];
};

struct mvdu_dt_settings {
	uint32_t timing_mode;
	bool mipi_enable;
	bool tv6xy_enable;
	bool cb_cr_swap_enable;
	bool luma_chroma_swap_enable;
	bool video_out_bypass_enable;
};

int mvdu_sched_next(const struct device *dev, uint32_t idx, struct video_buf *buf);
void mvdu_get_dt_cfg(const struct device *dev, struct mvdu_dt_settings *dest);

#endif /* _DCT_MVDU_H_ */
