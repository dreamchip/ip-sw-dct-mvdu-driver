/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#ifndef __MVDU_V4L_H__
#define __MVDU_V4L_H__

#include <media/v4l2-async.h>
#include <media/v4l2-common.h>
#include <media/v4l2-ctrls.h>
#include <media/v4l2-dev.h>
#include <media/v4l2-device.h>
#include <media/v4l2-dv-timings.h>
#include <media/v4l2-event.h>
#include <media/v4l2-ioctl.h>
#include <media/v4l2-mediabus.h>
#include <media/videobuf2-dma-contig.h>
#include <media/videobuf2-v4l2.h>
#include <linux/v4l2-dv-timings.h>
#include <linux/dma-mapping.h>
#include <linux/videodev2.h>

#ifdef CONFIG_MEDIA_CONTROLLER
#include <media/media-device.h>
#include <media/v4l2-mc.h>
#endif

#include "mvdu_core.h"

#define I2C_ADV7511 0x39
#define I2C_ADV7511_EDID 0x3f
#define I2C_ADV7511_CEC 0x3c
#define I2C_ADV7511_PKTMEM 0x38
#define I2C_ADV7511_BUS 0x08

#define MVDU_EDID_SIZE 256

enum {
	VID = 0, // video stream
	PIP = 1, // pictur-in-picture
	OSD = 2, // on-screen-display
	MAX_STREAMS,
};

enum mvdu_status {
	MVDU_IDLE,
	MVDU_INITIALISING,
	MVDU_RUNNING,
};

struct mvdu_fmt {
	uint32_t fourcc;
	char *desc;
	uint32_t code;
	uint8_t idx_mask;
	uint8_t planes;
};

struct mvdu_data;

struct mvdu_gpio_irq {
	unsigned gpio;
	unsigned active_low;
};

struct mvdu_stream {
	struct video_device vdev;
	struct vb2_queue queue;
	struct v4l2_pix_format_mplane pix_mp;
	struct v4l2_rect rect;
	struct list_head in_list;
	struct list_head out_list;
	struct mvdu_fmt *fmt;
	enum mvdu_status status;
	struct v4l2_dv_timings timings;
	uint32_t sequence;
	struct mutex fop_lock;
	struct v4l2_rect crop;
	struct v4l2_rect compose;
	struct v4l2_rect compose_default;
	struct v4l2_rect src;
	spinlock_t buf_lock;

	struct v4l2_device *v4l2_dev;
	struct v4l2_subdev *sd;
	uint32_t idx;
	struct media_pad pad;
	struct mvdu_data *rt;

	struct mutex ctrl_lock;
	struct v4l2_ctrl_handler ctrl_handler;
	struct v4l2_ctrl *ctrl_alpha;
	struct v4l2_ctrl *ctrl_bg_color;

	uint8_t alpha;

	struct mvdu_mif mif;
	int (*irq_handler)(uint32_t idx, void *irq_ctx);
	void *irq_ctx;
	bool enabled;
};

struct mvdu_stream_stat {
	uint32_t mem_urgent;
	uint32_t uflw;
	uint32_t frm;
};

struct mvdu_stat {
	uint32_t disp_finish;
	uint32_t line_treshold;
	uint32_t stop_vtg_finish;
	struct mvdu_stream_stat stream[MAX_STREAMS];
};

struct mvdu_data {
	struct platform_device *pdev;
	struct device *dev;
	struct v4l2_device v4l2_dev;
	struct mvdu_stream stream[MAX_STREAMS];
	struct mvdu_gpio_irq irq;
	struct work_struct work;
	struct v4l2_subdev *sd;
	struct v4l2_subdev *hdmi_in;
	uint8_t edid[MVDU_EDID_SIZE];
	struct v4l2_async_notifier notifier;
	uint32_t bgcolor;
	uint8_t alpha[MAX_STREAMS];

#ifdef CONFIG_MEDIA_CONTROLLER
	struct media_pad sink;
	struct media_device mdev;
#endif

	bool monitor_detected;
	bool adv7511_pd_polarity_active_high;

	struct clk *mclk;
	struct clk *pclk;
	struct mvdu_dif dif;
	struct mvdu_top top;
	struct mvdu_stat stat;
	uintptr_t fx_base;
	uintptr_t gdp_base;
	uintptr_t top_base;
	uint32_t pixelclock;
	struct zone_mem zone_mem;
	bool fpga;

	struct list_head zone_buf_list;
	struct mvdu_zone_buffer zone_buf[MVDU_ZONE_CHUNK_NUM];
};

#endif
