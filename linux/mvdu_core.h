/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#ifndef __MVDU_CORE_H__
#define __MVDU_CORE_H__

#include "mvdu_dif.h"
#include "mvdu_mi.h"

#define MVDU_DEF_IMAGE_WIDTH 1280
#define MVDU_DEF_IMAGE_HEIGHT 720

#define MVDU_ZONE_CHUNK_SIZE 4096
#define MVDU_ZONE_CHUNK_NUM 8
#define MVDU_ZONE_BUFFER_SIZE (MVDU_ZONE_CHUNK_SIZE * MVDU_ZONE_CHUNK_NUM)

struct mvdu_zone_buffer {
	uintptr_t base_virt;
	uintptr_t base_phys;
	struct list_head list;
};

struct mvdu_buffer {
	struct vb2_v4l2_buffer vb;
	struct list_head list;
	struct mvdu_zone_buffer *zb;
};

int mvdu_alloc_ressources(struct platform_device *pdev);
int mvdu_free_ressources(struct platform_device *pdev);

int mvdu_get_reg(struct platform_device *pdev, unsigned int addr);
int mvdu_set_reg(struct platform_device *pdev, unsigned int addr, u32 val);

int mvdu_sched_next(struct platform_device *pdev, u32 idx,
		    struct mvdu_buffer *buf);
int mvdu_start_stream(struct platform_device *pdev, u32 idx,
		      struct mvdu_buffer *buf);
int mvdu_stop_stream(struct platform_device *pdev, u32 idx);

int mvdu_register_irq_handler(struct platform_device *pdev, u32 idx,
			      int (*irq_handler)(uint32_t idx, void *irq_ctx),
			      void *irq_ctx);

int mvdu_enabled(struct platform_device *pdev);
int mvdu_probed(struct platform_device *pdev);

#endif
