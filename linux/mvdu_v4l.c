/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#undef DEBUG

#include <linux/delay.h>
#include <linux/platform_device.h>
#include <media/i2c/adv7511.h>

#include "mvdu_v4l.h"
#include "mvdu_structures.h"

static unsigned int debug;
module_param(debug, uint, 0644);
MODULE_PARM_DESC(debug, "debug level");

#define MIN_WIDTH 640
#define MIN_HEIGHT 480
#define MAX_WIDTH 1920
#define MAX_HEIGHT 1200
#define ALIGN_W 3
#define ALIGN_H 3
#define MIN_PIXELCLOCK 20000000
#define MAX_PIXELCLOCK 225000000

static const struct v4l2_dv_timings default_timing =
	V4L2_DV_BT_CEA_1920X1080P30;

#define MVDU_MODULE_NAME "mvdu-v4l"

#define dprintk(dev, lvl, fmt, arg...) \
	v4l2_dbg(lvl, debug, (dev)->v4l2_dev, "%s: " fmt, __func__, ##arg)

static const struct v4l2_dv_timings_cap mvdu_timings_cap = {
	.type = V4L2_DV_BT_656_1120,
	V4L2_INIT_BT_TIMINGS(MIN_WIDTH, MAX_WIDTH, MIN_HEIGHT, MAX_HEIGHT,
			     MIN_PIXELCLOCK, MAX_PIXELCLOCK,
			     V4L2_DV_BT_STD_CEA861 | V4L2_DV_BT_STD_DMT |
				     V4L2_DV_BT_STD_GTF | V4L2_DV_BT_STD_CVT,
			     V4L2_DV_BT_CAP_PROGRESSIVE |
				     V4L2_DV_BT_CAP_REDUCED_BLANKING |
				     V4L2_DV_BT_CAP_CUSTOM)
};

static struct mvdu_fmt mvdu_formats[] = {
	{
		.fourcc = V4L2_PIX_FMT_NV61M,
		.code = MEDIA_BUS_FMT_YUYV8_1X16,
		.desc = "Y/CbCr 4:2:2",
		.planes = 2,
		.idx_mask = BIT(1) | BIT(0),
	},
	{
		.fourcc = V4L2_PIX_FMT_ABGR32,
		.code = MEDIA_BUS_FMT_YUYV8_1X16,
		.desc = "BGRA-8-8-8-8",
		.planes = 1,
		.idx_mask = BIT(2),
	},
};

#define MVDU_NUM_FORMATS ARRAY_SIZE(mvdu_formats)

static const struct v4l2_frmsize_stepwise mvdu_frmsize_stepwise = {
	.min_width = MIN_WIDTH,
	.min_height = MIN_HEIGHT,
	.max_width = MAX_WIDTH,
	.max_height = MAX_HEIGHT,
	.step_width = 1UL << ALIGN_W,
	.step_height = 1UL << ALIGN_H,
};

static struct adv7511_platform_data adv7511_pdata = {
	.i2c_edid = 0x7e >> 1,
	.i2c_cec = 0x7c >> 1,
	.i2c_pktmem = 0x70 >> 1,
	.cec_clk = 12000000,
};
static struct i2c_board_info adv7511_info = {
	.type = "adv7511-v4l2",
	.addr = 0x39, /* 0x39 or 0x3d */
	.platform_data = &adv7511_pdata,
};

static inline struct mvdu_data *dev2priv(struct v4l2_device *v4l2_dev)
{
	return container_of(v4l2_dev, struct mvdu_data, v4l2_dev);
}

static inline struct mvdu_buffer *to_mvdu_buffer(struct vb2_v4l2_buffer *vb2)
{
	return container_of(vb2, struct mvdu_buffer, vb);
}

static char *mvdu_print_fourcc(u32 fmt)
{
	static char code[5];

	code[0] = (unsigned char)(fmt & 0xff);
	code[1] = (unsigned char)((fmt >> 8) & 0xff);
	code[2] = (unsigned char)((fmt >> 16) & 0xff);
	code[3] = (unsigned char)((fmt >> 24) & 0xff);
	code[4] = '\0';

	return code;
}

static void dump_resolution(int lvl, struct mvdu_stream *stream,
			    struct v4l2_rect *rect, const char *s)
{
	dprintk(stream, lvl, "pad %d - %s %dx%d,%dx%d\n", stream->idx, s,
		rect->left, rect->top, rect->width, rect->height);
}

static int mvdu_vidioc_querycap(struct file *file, void *priv_fh,
				struct v4l2_capability *vcap)
{
	struct video_device *vdev = video_devdata(file);
	struct mvdu_stream *stream = video_drvdata(file);

	dprintk(stream, 2, "pad %d\n", stream->idx);

	strlcpy(vcap->driver, MVDU_MODULE_NAME, sizeof(vcap->driver));
	strlcpy(vcap->card, MVDU_MODULE_NAME, sizeof(vcap->card));
	snprintf(vcap->bus_info, sizeof(vcap->bus_info), "platform:%s",
		 vdev->name);

	vcap->device_caps = V4L2_CAP_STREAMING | V4L2_CAP_READWRITE;
	vcap->device_caps |= V4L2_CAP_VIDEO_OUTPUT_MPLANE;
	vcap->device_caps |= V4L2_CAP_VIDEO_OUTPUT_OVERLAY;
	vcap->capabilities = vcap->device_caps | V4L2_CAP_DEVICE_CAPS;

	dprintk(stream, 2, "%x\n", vcap->capabilities);

	return 0;
}

static void mvdu_video_output_status_show(struct mvdu_stream *stream)
{
}

static int mvdu_vidioc_log_status(struct file *file, void *priv_fh)
{
	struct mvdu_stream *stream = video_drvdata(file);

	dprintk(stream, 2, "pad %d\n", stream->idx);

	mvdu_video_output_status_show(stream);

	if (stream->sd == NULL) {
		return 0;
	}

	v4l2_subdev_call(stream->sd, core, log_status);

	return 0;
}

#define CLIP(X) ((X) > 255 ? 255 : (X) < 0 ? 0 : X)

#define CRGB2Y(R, G, B) CLIP((19595 * R + 38470 * G + 7471 * B) >> 16)
#define CRGB2Cb(R, G, B)                                                      \
	CLIP((36962 * (B - CLIP((19595 * R + 38470 * G + 7471 * B) >> 16)) >> \
	      16) +                                                           \
	     128)
#define CRGB2Cr(R, G, B)                                                      \
	CLIP((46727 * (R - CLIP((19595 * R + 38470 * G + 7471 * B) >> 16)) >> \
	      16) +                                                           \
	     128)

static void mvdu_setup_memory_interface(struct mvdu_stream *stream)
{
	struct mvdu_mif mif;
	struct v4l2_bt_timings *bt = &stream->timings.bt;
	const unsigned min_height = MIN_HEIGHT;
	const unsigned min_width = MIN_WIDTH;
	struct v4l2_rect crop = stream->crop;
	struct v4l2_rect compose = stream->compose;

	dprintk(stream, 2, "pad %d\n", stream->idx);

	mif = stream->mif;

	dump_resolution(2, stream, &stream->src, "display resolution");
	dump_resolution(2, stream, &crop, "crop input window");
	dump_resolution(2, stream, &compose, "compose input window");

	if (crop.top == 0 && crop.left == 0 && crop.height == 0 &&
	    crop.width == 0) {
		crop = stream->src;
	} else {
		crop.top =
			clamp_t(unsigned, crop.top, 0, bt->height - min_height);
		crop.left =
			clamp_t(unsigned, crop.left, 0, bt->width - min_width);
		crop.height =
			clamp(crop.height, min_height, bt->height - crop.top);
		crop.width =
			clamp(crop.width, min_width, bt->width - crop.left);
	}

	dump_resolution(2, stream, &crop, "crop final window");

	mif.y.height = crop.height;
	mif.y.top = crop.top;
	mif.y.left = crop.left;
	mif.y.width = crop.width;
	mif.y.full_width = stream->src.width;
	mif.y.full_height = stream->src.height;

	mif.c = mif.y;

	mif.alpha = stream->alpha;

	dprintk(stream, 1, "pad %d, alpha 0x%x\n", stream->idx, stream->alpha);

	stream->mif = mif;
}

static void mvu_update_compose(struct mvdu_stream *stream)
{
	struct v4l2_rect compose = stream->compose;

	if (stream->idx == VID) {
		stream->rt->dif.act_hoffset = compose.left;
		stream->rt->dif.act_voffset = compose.top;
	} else if (stream->idx == PIP) {
		stream->rt->dif.pip_hoffset = compose.left;
		stream->rt->dif.pip_voffset = compose.top;
	}
}

static void mvdu_setup_display_interface(struct mvdu_stream *stream)
{
	struct mvdu_dif dif;
	struct v4l2_bt_timings *bt = &stream->timings.bt;
	int r, g, b;
	struct v4l2_rect compose = stream->compose;

	dif = stream->rt->dif;

	dif.act_height = bt->height;
	dif.act_width = bt->width;

	if (stream->idx == VID) {
		dif.act_hoffset = compose.left;
		dif.act_voffset = compose.top;
	} else if (stream->idx == PIP) {
		dif.pip_hoffset = compose.left;
		dif.pip_voffset = compose.top;
	}

	dif.h_back_porch = bt->hbackporch;
	dif.h_front_porch = bt->hfrontporch;
	dif.h_sync = bt->hsync;

	dif.v_back_porch = bt->vbackporch;
	dif.v_front_porch = bt->vfrontporch;
	dif.v_sync = bt->vsync;

	dif.pixelclock = bt->pixelclock;

	dif.hsync_polarity = (bt->polarities & V4L2_DV_HSYNC_POS_POL) ?
				     HIGH_ACTIVE :
				     LOW_ACTIVE;
	dif.vsync_polarity = (bt->polarities & V4L2_DV_VSYNC_POS_POL) ?
				     HIGH_ACTIVE :
				     LOW_ACTIVE;
	dif.fsync_polarity = 0;
	dif.pen_polarity = 0;

	r = stream->rt->bgcolor & 0xff;
	g = ((stream->rt->bgcolor >> 8) & 0xff);
	b = ((stream->rt->bgcolor >> 16) & 0xff);

	dif.def_y = CRGB2Y(r, g, b);
	dif.def_cb = CRGB2Cb(r, g, b);
	dif.def_cr = CRGB2Cr(r, g, b);

	dprintk(stream, 2,
		"width %d, hfrontporch %d, hsync %d, hbackporch %d\n",
		bt->width, bt->hfrontporch, bt->hsync, bt->hbackporch);
	dprintk(stream, 2,
		"height %d, vfrontporch %d, vsync %d, vbackporch %d\n",
		bt->height, bt->vfrontporch, bt->vsync, bt->vbackporch);
	dprintk(stream, 2, "hsyncpol %c, vsyncpol %c\n",
		(bt->polarities & V4L2_DV_HSYNC_POS_POL) ? '+' : '-',
		(bt->polarities & V4L2_DV_VSYNC_POS_POL) ? '+' : '-');

	stream->rt->dif = dif;
}

static int mvdu_vidioc_enum_dv_timings(struct file *file, void *priv_fh,
				       struct v4l2_enum_dv_timings *timings)
{
	// struct mvdu_stream *stream = video_drvdata(file);

	timings->pad = 0;

	return v4l2_enum_dv_timings_cap(timings, &mvdu_timings_cap, NULL, NULL);
}

static int mvdu_vidioc_s_dv_timings(struct file *file, void *priv_fh,
				    struct v4l2_dv_timings *timings)
{
	struct mvdu_stream *stream = video_drvdata(file);
	int ret;

	dprintk(stream, 2, "pad %d\n", stream->idx);

	if (!v4l2_valid_dv_timings(timings, &mvdu_timings_cap, NULL, NULL)) {
		return -EINVAL;
	}

	/* Fill the optional fields .standards and .flags in struct v4l2_dv_timings
	   if the format is one of the CEA or DMT timings. */
	v4l2_find_dv_timings_cap(timings, &mvdu_timings_cap, 0, NULL, NULL);

	if (vb2_is_busy(&stream->queue))
		return -EBUSY;

	stream->timings = *timings;
	mvdu_setup_display_interface(stream);

	if (stream->sd == NULL) {
		return 0;
	}

	ret = v4l2_subdev_call(stream->sd, video, s_dv_timings, timings);
	if (!ret) {
		dprintk(stream, 2, "width %d height %d\n", timings->bt.width,
			timings->bt.height);
	}

	return ret;
}

static int mvdu_vidioc_g_dv_timings(struct file *file, void *priv_fh,
				    struct v4l2_dv_timings *timings)
{
	struct mvdu_stream *stream = video_drvdata(file);

	dprintk(stream, 2, "pad %d\n", stream->idx);

	if (stream->sd == NULL) {
		*timings = stream->timings;
		return 0;
	}

	return v4l2_subdev_call(stream->sd, video, g_dv_timings, timings);
}

static int mvdu_vidioc_dv_timings_cap(struct file *file, void *priv_fh,
				      struct v4l2_dv_timings_cap *cap)
{
	// struct mvdu_stream *stream = video_drvdata(file);

	*cap = mvdu_timings_cap;
	return 0;
}

static struct mvdu_fmt *mvdu_find_format(u32 idx_mask, u32 fourcc)
{
	struct mvdu_fmt *fmt;
	unsigned int k;

	for (k = 0; k < MVDU_NUM_FORMATS; k++) {
		fmt = &mvdu_formats[k];
		if ((fmt->fourcc == fourcc) && (fmt->idx_mask & idx_mask))
			break;
	}

	if (k == MVDU_NUM_FORMATS)
		return NULL;

	return &mvdu_formats[k];
}

static int mvdu_vidioc_try_fmt_vid_out(struct file *file, void *priv_fh,
				       struct v4l2_format *f)
{
	struct mvdu_stream *stream = video_drvdata(file);
	struct v4l2_pix_format_mplane *pix_mp = &f->fmt.pix_mp;
	struct mvdu_fmt *fmt;

	fmt = mvdu_find_format(BIT(stream->idx), pix_mp->pixelformat);
	if (!fmt) {
		fmt = &mvdu_formats[0];
	}

	v4l2_apply_frmsize_constraints(&pix_mp->width, &pix_mp->height,
				       &mvdu_frmsize_stepwise);
	v4l2_fill_pixfmt_mp(pix_mp, fmt->fourcc, pix_mp->width, pix_mp->height);
	pix_mp->field = V4L2_FIELD_NONE;

	//max_width = stream->timings.bt.width;
	//max_height = stream->timings.bt.height;

	if (pix_mp->colorspace == V4L2_COLORSPACE_DEFAULT)
		pix_mp->colorspace = V4L2_COLORSPACE_REC709;
	if (pix_mp->xfer_func == V4L2_XFER_FUNC_DEFAULT)
		pix_mp->xfer_func =
			V4L2_MAP_XFER_FUNC_DEFAULT(pix_mp->colorspace);
	if (pix_mp->ycbcr_enc == V4L2_YCBCR_ENC_DEFAULT)
		pix_mp->ycbcr_enc =
			V4L2_MAP_YCBCR_ENC_DEFAULT(pix_mp->colorspace);
	if (pix_mp->quantization == V4L2_QUANTIZATION_DEFAULT)
		pix_mp->quantization = V4L2_MAP_QUANTIZATION_DEFAULT(
			false, pix_mp->colorspace, pix_mp->ycbcr_enc);

	dprintk(stream, 1, "%s, %dx%d\n",
		mvdu_print_fourcc(pix_mp->pixelformat), pix_mp->width,
		pix_mp->height);

	return 0;
}

static int mvdu_vidioc_g_fmt_vid_out(struct file *file, void *priv_fh,
				     struct v4l2_format *f)
{
	struct mvdu_stream *stream = video_drvdata(file);
	struct v4l2_pix_format_mplane *pix_mp = &f->fmt.pix_mp;

	dprintk(stream, 2, "pad %d\n", stream->idx);

	f->type = V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
	*pix_mp = stream->pix_mp;

	dprintk(stream, 1, "%s, %dx%d\n",
		mvdu_print_fourcc(f->fmt.pix_mp.pixelformat),
		f->fmt.pix_mp.width, f->fmt.pix_mp.height);

	return 0;
}

static int mvdu_vidioc_enum_fmt_vid_out(struct file *file, void *priv_fh,
					struct v4l2_fmtdesc *f)
{
	struct mvdu_stream *stream = video_drvdata(file);
	const struct mvdu_fmt *fmt;
	int i, num = 0;

	dprintk(stream, 2, "pad %d\n", f->index);

	for (i = 0; i < ARRAY_SIZE(mvdu_formats); i++) {
		fmt = &mvdu_formats[i];
		if (fmt->idx_mask & BIT(stream->idx)) {
			if (num == f->index) {
				f->pixelformat = fmt->fourcc;
				return 0;
			}
			++num;
		}
	}

	return -EINVAL;
}

static int mvdu_vidioc_s_fmt_vid_out(struct file *file, void *priv_fh,
				     struct v4l2_format *f)
{
	struct mvdu_stream *stream = video_drvdata(file);
	struct v4l2_pix_format_mplane *pix_mp = &f->fmt.pix_mp;
	struct mvdu_fmt *fmt;
	struct v4l2_subdev_format sd_fmt = {
		.which = V4L2_SUBDEV_FORMAT_ACTIVE
	};

	dprintk(stream, 2, "pad %d\n", stream->idx);

	if (vb2_is_busy(&stream->queue))
		return -EBUSY;

	if (mvdu_vidioc_try_fmt_vid_out(file, priv_fh, f))
		return -EINVAL;

	fmt = mvdu_find_format(BIT(stream->idx), pix_mp->pixelformat);

	if (stream->sd && stream->idx < 2) {
		v4l2_fill_mbus_format_mplane(&sd_fmt.format, pix_mp);
		v4l2_subdev_call(stream->sd, pad, set_fmt, NULL, &sd_fmt);
	}

	stream->fmt = fmt;
	stream->pix_mp = f->fmt.pix_mp;

	stream->src.width = pix_mp->width;
	stream->src.height = pix_mp->height;
	stream->src.left = 0;
	stream->src.top = 0;

	dump_resolution(2, stream, &stream->src, "video input window");

	return 0;
}

static int mvdu_vidioc_s_fmt_again(struct mvdu_stream *stream)
{
	struct v4l2_subdev_format sd_fmt = {
		.which = V4L2_SUBDEV_FORMAT_ACTIVE
	};

	dprintk(stream, 2, "pad %d\n", stream->idx);

	v4l2_fill_mbus_format_mplane(&sd_fmt.format, &stream->pix_mp);
	if (stream->sd && stream->idx < 2)
		v4l2_subdev_call(stream->sd, pad, set_fmt, NULL, &sd_fmt);

	return 0;
}

static int mvdu_vidioc_enum_output(struct file *file, void *priv_fh,
				   struct v4l2_output *out)
{
	struct mvdu_stream *stream = video_drvdata(file);

	if (out->index)
		return -EINVAL;

	dprintk(stream, 2, "pad %d\n", stream->idx);

	snprintf(out->name, sizeof(out->name), "HDMI-%d", out->index);
	out->type = V4L2_OUTPUT_TYPE_ANALOG;
	out->capabilities = V4L2_OUT_CAP_DV_TIMINGS;

	return 0;
}

static int mvdu_vidioc_g_output(struct file *file, void *priv_fh,
				unsigned int *i)
{
	*i = 0;
	return 0;
}

static int mvdu_vidioc_s_output(struct file *file, void *priv_fh,
				unsigned int i)
{
	return i ? -EINVAL : 0;
}

static int mvdu_vidioc_g_edid(struct file *file, void *fh,
			      struct v4l2_edid *edid)
{
	struct mvdu_stream *stream = video_drvdata(file);
	u32 pad = edid->pad;
	int ret;

	dprintk(stream, 2, "");

	if (stream->sd == NULL || pad) {
		return -EINVAL;
	}

	ret = v4l2_subdev_call(stream->sd, pad, get_edid, edid);

	return ret;
}

static int
mvdu_vidioc_subscribe_event(struct v4l2_fh *fh,
			    const struct v4l2_event_subscription *sub)
{
	switch (sub->type) {
	case V4L2_EVENT_SOURCE_CHANGE:
		return v4l2_event_subscribe(fh, sub, 4, NULL);
	}

	return v4l2_ctrl_subscribe_event(fh, sub);
}

static int mvdu_vidioc_s_selection(struct file *file, void *priv,
				   struct v4l2_selection *sel)
{
	struct mvdu_stream *stream = video_drvdata(file);
	struct v4l2_bt_timings *bt = &stream->timings.bt;
	unsigned min_height = MIN_HEIGHT;
	unsigned min_width = MIN_WIDTH;

	dprintk(stream, 2, "type: %#x, target: %#x, flags: %#x\n", sel->type,
		sel->target, sel->flags);
	dump_resolution(2, stream, &sel->r, "set window");

	if (sel->type != V4L2_BUF_TYPE_VIDEO_OUTPUT &&
	    sel->type != V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE)
		return -EINVAL;

	switch (sel->target) {
	case V4L2_SEL_TGT_CROP_BOUNDS:
	case V4L2_SEL_TGT_CROP_DEFAULT:
	case V4L2_SEL_TGT_COMPOSE_BOUNDS:
		sel->r.top = sel->r.left = 0;
		sel->r.width = bt->width;
		sel->r.height = bt->height;
		dump_resolution(2, stream, &sel->r, "reset default window");
		break;
	case V4L2_SEL_TGT_COMPOSE_DEFAULT:
		stream->compose_default = sel->r;
		dump_resolution(2, stream, &stream->compose_default,
				"set default compose window");
		break;
	case V4L2_SEL_TGT_COMPOSE:
		sel->r.width = clamp(sel->r.width, 16U, bt->width);
		sel->r.height = clamp(sel->r.height, 16U, bt->height);
		sel->r.left = clamp_t(unsigned, sel->r.left, 0,
				      bt->width - sel->r.width);
		sel->r.top = clamp_t(unsigned, sel->r.top, 0,
				     bt->height - sel->r.height);
		stream->compose = sel->r;
		dump_resolution(2, stream, &stream->compose,
				"set compose window");
		break;
	case V4L2_SEL_TGT_CROP:
		sel->r.top = clamp_t(unsigned, sel->r.top, 0,
				     bt->height - min_height);
		sel->r.left = clamp_t(unsigned, sel->r.left, 0,
				      bt->width - min_width);
		sel->r.height = clamp(sel->r.height, min_height,
				      bt->height - sel->r.top);
		sel->r.width =
			clamp(sel->r.width, min_width, bt->width - sel->r.left);
		stream->crop = sel->r;
		dump_resolution(2, stream, &stream->crop,
				"set cropping window");
		break;
	default:
		dprintk(stream, 2, "unknown target 0x%x\n", sel->target);
		return -EINVAL;
	}

	return 0;
}

static int mvdu_vidioc_g_selection(struct file *file, void *priv,
				   struct v4l2_selection *sel)
{
	struct mvdu_stream *stream = video_drvdata(file);
	struct v4l2_bt_timings *bt = &stream->timings.bt;

	dprintk(stream, 2, "type 0x%x, target 0x%x\n", sel->type, sel->target);

	if (sel->type != V4L2_BUF_TYPE_VIDEO_OUTPUT &&
	    sel->type != V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE)
		return -EINVAL;

	switch (sel->target) {
	case V4L2_SEL_TGT_CROP_BOUNDS:
	case V4L2_SEL_TGT_CROP_DEFAULT:
	case V4L2_SEL_TGT_COMPOSE_BOUNDS:
		sel->r.top = sel->r.left = 0;
		sel->r.width = bt->width;
		sel->r.height = bt->height;
		dump_resolution(2, stream, &sel->r, "get default window");
		break;
	case V4L2_SEL_TGT_COMPOSE_DEFAULT:
		sel->r = stream->compose_default;
		dump_resolution(2, stream, &sel->r,
				"get default compose window");
		break;
	case V4L2_SEL_TGT_COMPOSE:
		sel->r = stream->compose;
		dump_resolution(2, stream, &sel->r, "get compose window");
		break;
	case V4L2_SEL_TGT_CROP:
		sel->r = stream->crop;
		dump_resolution(2, stream, &sel->r, "get crop window");
		break;
	default:
		dprintk(stream, 2, "unknown target 0x%x\n", sel->target);
		return -EINVAL;
	}

	return 0;
}

#ifdef CONFIG_VIDEO_ADV_DEBUG
static int mvdu_reg(struct platform_device *pdev, unsigned int cmd, void *arg)
{
	struct v4l2_dbg_register *regs = arg;

	if (!capable(CAP_SYS_ADMIN))
		return -EPERM;

	regs->size = 4;
	if (cmd == VIDIOC_DBG_S_REGISTER)
		mvdu_set_reg(pdev, regs->reg, regs->val);
	else
		regs->val = mvdu_get_reg(pdev, regs->reg);
	return 0;
}

static int mvdu_vidioc_g_register(struct file *file, void *priv_fh,
				  struct v4l2_dbg_register *reg)
{
	struct mvdu_stream *stream = video_drvdata(file);

	return mvdu_reg(stream->rt->pdev, VIDIOC_DBG_G_REGISTER, reg);
}

static int mvdu_vidioc_s_register(struct file *file, void *priv_fh,
				  const struct v4l2_dbg_register *reg)
{
	struct mvdu_stream *stream = video_drvdata(file);

	return mvdu_reg(stream->rt->pdev, VIDIOC_DBG_S_REGISTER,
			(struct v4l2_dbg_register *)reg);
}
#endif

/* Locking: caller holds fop_lock mutex */
static int mvdu_queue_setup(struct vb2_queue *q, unsigned int *nbuffers,
			    unsigned int *nplanes, unsigned int sizes[],
			    struct device *alloc_devs[])
{
	struct mvdu_stream *stream = vb2_get_drv_priv(q);
	const struct v4l2_pix_format_mplane *format;
	int i;

	dprintk(stream, 1, "requesting %d buffers, %d planes\n", *nbuffers,
		*nplanes);

	format = &stream->pix_mp;

	if (*nplanes) {
		if (*nplanes != format->num_planes) {
			dprintk(stream, 1, "providing %d, required %d planes\n",
				*nplanes, format->num_planes);
			return -EINVAL;
		}

		for (i = 0; i < *nplanes; ++i) {
			if (sizes[i] < format->plane_fmt[i].sizeimage) {
				dprintk(stream, 1,
					"providing %d, required %d bytes\n",
					sizes[i],
					format->plane_fmt[i].sizeimage);
				return -EINVAL;
			}
			sizes[i] = PAGE_ALIGN(format->plane_fmt[i].sizeimage);
		}

		return 0;
	} else {
		*nplanes = format->num_planes;
	}

	for (i = 0; i < *nplanes; ++i) {
		sizes[i] = format->plane_fmt[i].sizeimage;
		dprintk(stream, 1, "plane %d, allocate %d bytes\n", i,
			sizes[i]);
	}

	return 0;
}

static int mvdu_buf_prepare(struct vb2_buffer *vb)
{
	struct mvdu_stream *stream = vb2_get_drv_priv(vb->vb2_queue);
	int i;

	dprintk(stream, 2, "pad %d\n", stream->idx);

	for (i = 0; i < stream->pix_mp.num_planes; i++) {
		uint32_t size = stream->pix_mp.plane_fmt[i].sizeimage;
		if (vb2_plane_size(vb, i) < size) {
			dprintk(stream, 2,
				"data will not fit into plane (%lu < %lu)\n",
				vb2_plane_size(vb, i), (long)size);
			return -EINVAL;
		}
		vb2_set_plane_payload(vb, i, size);
	}

	return 0;
}

/* Locking: caller holds fop_lock mutex and vq->irqlock spinlock */
static void mvdu_buf_queue(struct vb2_buffer *vb)
{
	struct vb2_v4l2_buffer *vbuf = to_vb2_v4l2_buffer(vb);
	struct mvdu_stream *stream = vb2_get_drv_priv(vb->vb2_queue);
	struct mvdu_buffer *buf = to_mvdu_buffer(vbuf);
	unsigned long flags;

	dprintk(stream, 2, "pad %d\n", stream->idx);

	spin_lock_irqsave(&stream->buf_lock, flags);

	list_add_tail(&buf->list, &stream->in_list);

	if (stream->idx == OSD && !list_empty(&stream->rt->zone_buf_list)) {
		buf->zb = list_entry(stream->rt->zone_buf_list.next,
				     struct mvdu_zone_buffer, list);
		list_del(&buf->zb->list);
	} else {
		buf->zb = NULL;
	}

	spin_unlock_irqrestore(&stream->buf_lock, flags);
}

static int mvdu_start_streaming(struct vb2_queue *q, unsigned int count)
{
	struct mvdu_stream *stream = vb2_get_drv_priv(q);
	struct mvdu_buffer *buf, *node;
	int ret;

	dprintk(stream, 2, "pad %d\n", stream->idx);

	stream->sequence = 0;

	if (!mvdu_enabled(stream->rt->pdev)) {
		mvdu_setup_display_interface(stream);

		ret = v4l2_device_call_until_err(stream->v4l2_dev, 0, video,
						 s_stream, 1);
		if (ret < 0 && ret != -ENOIOCTLCMD)
			goto fail;

		mvdu_vidioc_s_fmt_again(stream);
	} else {
		mvu_update_compose(stream);
	}

	mvdu_setup_memory_interface(stream);

	buf = list_entry(stream->in_list.next, struct mvdu_buffer, list);
	mvdu_start_stream(stream->rt->pdev, stream->idx, buf);
	stream->status = MVDU_RUNNING;

	dprintk(stream, 2, "done\n");

	return 0;

fail:
	list_for_each_entry_safe(buf, node, &stream->in_list, list) {
		vb2_buffer_done(&buf->vb.vb2_buf, VB2_BUF_STATE_QUEUED);
		list_del(&buf->list);
	}
	list_for_each_entry_safe(buf, node, &stream->out_list, list) {
		vb2_buffer_done(&buf->vb.vb2_buf, VB2_BUF_STATE_QUEUED);
		list_del(&buf->list);
	}

	return ret;
}

static void mvdu_stop_streaming(struct vb2_queue *q)
{
	struct mvdu_stream *stream = vb2_get_drv_priv(q);
	struct mvdu_buffer *buf, *node;
	unsigned long flags;

	dprintk(stream, 2, "pad %d\n", stream->idx);

	mvdu_stop_stream(stream->rt->pdev, stream->idx);

	if (!mvdu_enabled(stream->rt->pdev))
		v4l2_device_call_until_err(stream->v4l2_dev, 0, video, s_stream,
					   0);

	msleep(50);
	spin_lock_irqsave(&stream->buf_lock, flags);
	list_for_each_entry_safe(buf, node, &stream->in_list, list) {
		vb2_buffer_done(&buf->vb.vb2_buf, VB2_BUF_STATE_ERROR);
		list_del(&buf->list);
	}
	list_for_each_entry_safe(buf, node, &stream->out_list, list) {
		vb2_buffer_done(&buf->vb.vb2_buf, VB2_BUF_STATE_ERROR);
		list_del(&buf->list);
	}
	spin_unlock_irqrestore(&stream->buf_lock, flags);

	dprintk(stream, 2, "done\n");
}

int mvdu_mvdu_irq_handler(uint32_t idx, void *irq_ctx)
{
	struct mvdu_stream *stream;
	struct mvdu_buffer *vb;

	stream = irq_ctx;

	dprintk(stream, 2, "pad %d\n", stream->idx);

	spin_lock(&stream->buf_lock);

	if (list_empty(&stream->in_list)) {
		spin_unlock(&stream->buf_lock);
		return 0;
	}

	if (!list_empty(&stream->out_list)) {
		dprintk(stream, 2, "pad %d - buffer done\n", stream->idx);
		vb = list_entry(stream->out_list.next, struct mvdu_buffer,
				list);
		list_del(&vb->list);
		if (vb->zb)
			list_add_tail(&vb->zb->list,
				      &stream->rt->zone_buf_list);
		vb2_buffer_done(&vb->vb.vb2_buf, VB2_BUF_STATE_DONE);
	}

	vb = list_entry(stream->in_list.next, struct mvdu_buffer, list);

	if (list_is_singular(&stream->in_list)) {
		dprintk(stream, 2, "pad %d - re-use old buffer\n", stream->idx);

		if (vb->vb.sequence < stream->sequence) {
			v4l2_err(stream->v4l2_dev,
				 "sequence error, got %u >= want %u\n",
				 vb->vb.sequence, stream->sequence);
		}
		stream->sequence = vb->vb.sequence;

		/* Keep cycling while no next buffer is available */
		mvdu_sched_next(stream->rt->pdev, stream->idx, vb);
		spin_unlock(&stream->buf_lock);
		return 0;
	}

	dprintk(stream, 2, "pad %d - next buffer\n", stream->idx);

	list_del(&vb->list);
	list_add_tail(&vb->list, &stream->out_list);

	vb = list_entry(stream->in_list.next, struct mvdu_buffer, list);

	if (vb->vb.sequence < stream->sequence) {
		v4l2_err(stream->v4l2_dev,
			 "sequence error, got %u >= want %u\n", vb->vb.sequence,
			 stream->sequence);
	}
	stream->sequence = vb->vb.sequence;

	mvdu_sched_next(stream->rt->pdev, stream->idx, vb);

	spin_unlock(&stream->buf_lock);

	return 0;
}

static const struct vb2_ops mvdu_qops = {
	.buf_prepare = mvdu_buf_prepare,
	.buf_queue = mvdu_buf_queue,
	.queue_setup = mvdu_queue_setup,
	.start_streaming = mvdu_start_streaming,
	.stop_streaming = mvdu_stop_streaming,
	.wait_finish = vb2_ops_wait_finish,
	.wait_prepare = vb2_ops_wait_prepare,
};

/* File operations */
static int mvdu_open(struct file *file)
{
	struct mvdu_stream *stream = video_drvdata(file);
	int ret;

	dprintk(stream, 2, "");

	if (mutex_lock_interruptible(&stream->fop_lock))
		return -ERESTARTSYS;

	ret = v4l2_fh_open(file);
	if (ret)
		goto done_open;
	if (v4l2_fh_is_singular_file(file) &&
	    stream->status == MVDU_INITIALISING) {
		/* First open */
		stream->status = MVDU_IDLE;
	}

done_open:
	mutex_unlock(&stream->fop_lock);

	dprintk(stream, 2, "done\n");

	return ret;
}

static int mvdu_release(struct file *file)
{
	struct mvdu_stream *stream = video_drvdata(file);
	bool is_last;

	dprintk(stream, 2, "");

	mutex_lock(&stream->fop_lock);
	is_last = v4l2_fh_is_singular_file(file);
	_vb2_fop_release(file, NULL);
	if (is_last) {
		/* Last close */
		stream->status = MVDU_INITIALISING;
	}
	mutex_unlock(&stream->fop_lock);

	dprintk(stream, 2, "done\n");

	return 0;
}

static const struct v4l2_ioctl_ops mvdu_ioctl_ops = {
	.vidioc_querycap = mvdu_vidioc_querycap,

	.vidioc_log_status = mvdu_vidioc_log_status,

	.vidioc_enum_output = mvdu_vidioc_enum_output,
	.vidioc_g_output = mvdu_vidioc_g_output,
	.vidioc_s_output = mvdu_vidioc_s_output,

	.vidioc_enum_fmt_vid_out = mvdu_vidioc_enum_fmt_vid_out,
	.vidioc_g_fmt_vid_out_mplane = mvdu_vidioc_g_fmt_vid_out,
	.vidioc_s_fmt_vid_out_mplane = mvdu_vidioc_s_fmt_vid_out,
	.vidioc_try_fmt_vid_out_mplane = mvdu_vidioc_try_fmt_vid_out,

	.vidioc_create_bufs = vb2_ioctl_create_bufs,
	.vidioc_dqbuf = vb2_ioctl_dqbuf,
	.vidioc_expbuf = vb2_ioctl_expbuf,
	.vidioc_prepare_buf = vb2_ioctl_prepare_buf,
	.vidioc_qbuf = vb2_ioctl_qbuf,
	.vidioc_querybuf = vb2_ioctl_querybuf,
	.vidioc_reqbufs = vb2_ioctl_reqbufs,

	.vidioc_g_edid = mvdu_vidioc_g_edid,

	.vidioc_dv_timings_cap = mvdu_vidioc_dv_timings_cap,
	.vidioc_enum_dv_timings = mvdu_vidioc_enum_dv_timings,
	.vidioc_g_dv_timings = mvdu_vidioc_g_dv_timings,
	.vidioc_s_dv_timings = mvdu_vidioc_s_dv_timings,

	.vidioc_g_selection = mvdu_vidioc_g_selection,
	.vidioc_s_selection = mvdu_vidioc_s_selection,

	.vidioc_subscribe_event = mvdu_vidioc_subscribe_event,
	.vidioc_unsubscribe_event = v4l2_event_unsubscribe,

	.vidioc_streamoff = vb2_ioctl_streamoff,
	.vidioc_streamon = vb2_ioctl_streamon,

#ifdef CONFIG_VIDEO_ADV_DEBUG
	.vidioc_g_register = mvdu_vidioc_g_register,
	.vidioc_s_register = mvdu_vidioc_s_register,
#endif
};

static const struct v4l2_file_operations mvdu_fops = {
	.owner = THIS_MODULE,
	.open = mvdu_open,
	.release = mvdu_release,
	.unlocked_ioctl = video_ioctl2,
	.mmap = vb2_fop_mmap,
	.poll = vb2_fop_poll,
	.write = vb2_fop_write,
};

#define V4L2_CID_USER_DCT_BASE (V4L2_CID_USER_BASE + 0x1200)
#define V4L2_CID_DCT_ALPHA (V4L2_CID_USER_DCT_BASE + 1)
#define V4L2_CID_DCT_BGCOLOR (V4L2_CID_USER_DCT_BASE + 2)

static int mvdu_set_ctrl(struct v4l2_ctrl *ctrl)
{
	struct mvdu_stream *stream =
		container_of(ctrl->handler, struct mvdu_stream, ctrl_handler);

	if (stream == NULL)
		return -EINVAL;

	switch (ctrl->id) {
	case V4L2_CID_DCT_BGCOLOR:
		stream->rt->bgcolor = ctrl->p_new.p_u32[0];
		dprintk(stream, 1, "pad %d, id 0x%x, val 0x%x\n", stream->idx,
			ctrl->id, stream->rt->bgcolor);
		break;

	case V4L2_CID_DCT_ALPHA:
		stream->alpha = ctrl->p_new.p_u8[0];
		stream->rt->alpha[stream->idx] = stream->alpha;
		dprintk(stream, 1, "pad %d, id 0x%x, val 0x%x\n", stream->idx,
			ctrl->id, stream->alpha);
		break;
	}

	return 0;
}

static int mvdu_get_ctrl(struct v4l2_ctrl *ctrl)
{
	struct mvdu_stream *stream =
		container_of(ctrl->handler, struct mvdu_stream, ctrl_handler);

	dprintk(stream, 1, "pad %d, id 0x%x\n", stream->idx, ctrl->id);

	if (stream == NULL)
		return -EINVAL;

	switch (ctrl->id) {
	case V4L2_CID_DCT_BGCOLOR:
		ctrl->cur.val = stream->rt->bgcolor;
		break;

	case V4L2_CID_DCT_ALPHA:
		ctrl->cur.val = stream->alpha;
		break;
	}

	return 0;
}

static const struct v4l2_ctrl_ops mvdu_ctrl_ops = {
	.s_ctrl = mvdu_set_ctrl,
	.g_volatile_ctrl = mvdu_get_ctrl,
};

struct v4l2_ctrl_config mvdu_ctrl_alpha_config = {
	.ops = &mvdu_ctrl_ops,
	.id = V4L2_CID_DCT_ALPHA,
	.type = V4L2_CTRL_TYPE_U8,
	.name = "alpha",
	.min = 0x00,
	.max = 0xff,
	.step = 1,
	.def = 0xff,
	.dims = { 1 },
	.flags = V4L2_CTRL_FLAG_EXECUTE_ON_WRITE,
};

struct v4l2_ctrl_config mvdu_ctrl_bgcolor_config = {
	.ops = &mvdu_ctrl_ops,
	.id = V4L2_CID_DCT_BGCOLOR,
	.type = V4L2_CTRL_TYPE_U32,
	.name = "bgcolor",
	.min = 0x00000000,
	.max = 0xffffffff,
	.step = 1,
	.def = 0x00808080,
	.dims = { 1 },
	.flags = V4L2_CTRL_FLAG_EXECUTE_ON_WRITE,
};

static int mvdu_subdevs_init(struct mvdu_data *rt)
{
	int ret;

	ret = v4l2_subdev_call(rt->sd, core, s_power, 1);
	if (ret != 1) {
		pr_err("rt: can't enable power\n");
		return ret;
	}
	ret = v4l2_subdev_call(rt->sd, video, s_stream, 1);
	if (ret) {
		pr_err("rt: can't enable video stream\n");
		return ret;
	}
	ret = v4l2_subdev_call(rt->sd, audio, s_stream, 0);
	if (ret) {
		pr_err("rt: can't disable audio stream\n");
		return ret;
	}
	ret = v4l2_ctrl_s_ctrl(v4l2_ctrl_find(rt->sd->ctrl_handler,
					      V4L2_CID_DV_TX_MODE),
			       V4L2_DV_TX_MODE_HDMI);
	if (ret) {
		pr_err("rt: can't enable HDMI\n");
		return ret;
	}

	return 0;
}

static void mvdu_get_of_config(struct platform_device *pdev,
			       struct mvdu_data *rt)
{
	struct device_node *dn = pdev->dev.of_node;
	u32 addr = 0x39;

	if (!dn)
		return;

	of_property_read_u32(dn, "adv7511-addr", &addr);
	adv7511_info.addr = addr;
	if (addr == 0x3d)
		rt->adv7511_pd_polarity_active_high = false;
	else
		rt->adv7511_pd_polarity_active_high = true;
}

static void mvdu_notify(struct v4l2_subdev *sd, unsigned int notification,
			void *arg)
{
	struct mvdu_data *rt = dev2priv(sd->v4l2_dev);
	union notify_data {
		struct adv7511_monitor_detect *md;
		struct adv7511_edid_detect *ed;
	} nd;

	switch (notification) {
	case ADV7511_MONITOR_DETECT:
		pr_err("adv7511 monitor detected\n");
		rt->monitor_detected = true;
		break;
	case ADV7511_EDID_DETECT:
		nd.ed = arg;
		pr_err("adv7511 edid %sdetected", nd.ed->present ? "" : "not ");
		if (!nd.ed->present)
			break;
		break;
	default:
		pr_err("adv7511 oops\n");
		break;
	}
}

static int mvdu_init_stream(struct platform_device *pdev, struct mvdu_data *rt,
			    struct mvdu_stream *stream)
{
	struct v4l2_pix_format_mplane *pix_mp;

	stream->status = MVDU_INITIALISING;

	pix_mp = &stream->pix_mp;

	stream->fmt = &mvdu_formats[0];
	pix_mp = &stream->pix_mp;
	pix_mp->field = V4L2_FIELD_NONE;
	pix_mp->colorspace = V4L2_COLORSPACE_REC709;
	//	pix_mp->colorspace = V4L2_COLORSPACE_SRGB;
	pix_mp->xfer_func = V4L2_MAP_XFER_FUNC_DEFAULT(pix_mp->colorspace);
	pix_mp->ycbcr_enc = V4L2_MAP_YCBCR_ENC_DEFAULT(pix_mp->colorspace);
	pix_mp->quantization = V4L2_MAP_QUANTIZATION_DEFAULT(
		false, pix_mp->colorspace, pix_mp->ycbcr_enc);
	v4l2_fill_pixfmt_mp(pix_mp, stream->fmt->fourcc, MVDU_DEF_IMAGE_WIDTH,
			    MVDU_DEF_IMAGE_HEIGHT);

	INIT_LIST_HEAD(&stream->in_list);
	INIT_LIST_HEAD(&stream->out_list);
	spin_lock_init(&stream->buf_lock);
	mutex_init(&stream->fop_lock);

	stream->v4l2_dev = &rt->v4l2_dev;

	stream->alpha = rt->alpha[stream->idx];

	mutex_init(&stream->ctrl_lock);
	stream->ctrl_handler.lock = &stream->ctrl_lock;
	v4l2_ctrl_handler_init(&stream->ctrl_handler, 2);

	stream->ctrl_alpha = v4l2_ctrl_new_custom(
		&stream->ctrl_handler, &mvdu_ctrl_alpha_config, NULL);
	if (stream->ctrl_handler.error) {
		pr_err("v4l2_ctrl_new_custom failed (V4L2_CID_DCT_ALPHA): %d\n",
		       stream->ctrl_handler.error);
		return stream->ctrl_handler.error;
	}

	stream->ctrl_bg_color = v4l2_ctrl_new_custom(
		&stream->ctrl_handler, &mvdu_ctrl_bgcolor_config, NULL);
	if (stream->ctrl_handler.error) {
		pr_err("v4l2_ctrl_new_custom failed (V4L2_CID_DCT_BGCOLOR): %d\n",
		       stream->ctrl_handler.error);
		return stream->ctrl_handler.error;
	}
	stream->vdev.ctrl_handler = &stream->ctrl_handler;

	return 0;
}

static int mvdu_init(struct platform_device *pdev, struct mvdu_data *rt)
{
	int ret, i;

	rt->pdev = pdev;

	rt->bgcolor = 0x00808080;
	rt->alpha[0] = 0;
	rt->alpha[1] = 0xff;
	rt->alpha[2] = 0x80;

	for (i = 0; i < MAX_STREAMS; i++) {
		rt->stream[i].idx = i;
		rt->stream[i].rt = rt;

		mvdu_init_stream(pdev, rt, &rt->stream[i]);

		ret = mvdu_register_irq_handler(pdev, i, mvdu_mvdu_irq_handler,
						&rt->stream[i]);
		if (ret < 0) {
			pr_err("can't register MVDU interrupt\n");
			return -1;
		}
	}

	return 0;
}

static int mvdu_init_queue(struct mvdu_data *rt, struct mvdu_stream *stream)
{
	struct vb2_queue *q;
	struct video_device *vdev;
	int ret;

	vdev = &stream->vdev;

	/* Initialize the vb2 queue */
	q = &stream->queue;
	q->type = V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
	q->io_modes = VB2_MMAP | VB2_DMABUF | VB2_WRITE | VB2_USERPTR;
	q->drv_priv = stream;
	q->buf_struct_size = sizeof(struct mvdu_buffer);
	q->ops = &mvdu_qops;
	q->mem_ops = &vb2_dma_contig_memops;
	q->timestamp_flags = V4L2_BUF_FLAG_TIMESTAMP_MONOTONIC;
	q->min_buffers_needed = 2;
	q->lock = &stream->fop_lock;
	q->dev = &rt->pdev->dev;
	ret = vb2_queue_init(q);
	if (ret)
		return -1;
	vdev->queue = q;

	return 0;
}

static int mvdu_stream_register(struct mvdu_data *rt, u32 node)
{
	struct video_device *vdev;
	struct mvdu_stream *stream = rt->stream + node;
	int ret;

	vdev = &stream->vdev;

	snprintf(vdev->name, sizeof(vdev->name), "%s-%d", rt->v4l2_dev.name,
		 node);

	vdev->device_caps = V4L2_CAP_STREAMING | V4L2_CAP_READWRITE;
	vdev->device_caps |= V4L2_CAP_VIDEO_OUTPUT_MPLANE;
	vdev->device_caps |= V4L2_CAP_VIDEO_OUTPUT_OVERLAY;
	vdev->fops = &mvdu_fops;
	vdev->ioctl_ops = &mvdu_ioctl_ops;
	vdev->lock = &stream->fop_lock;
	vdev->release = video_device_release_empty;
	vdev->v4l2_dev = &rt->v4l2_dev;
	vdev->vfl_dir = VFL_DIR_TX;

	stream->timings = default_timing;
	stream->pad.flags = MEDIA_PAD_FL_SINK;

	video_set_drvdata(vdev, stream);

	ret = mvdu_init_queue(rt, stream);
	if (ret < 0) {
		v4l2_err(&rt->v4l2_dev, "Failed to init queue\n");
		return ret;
	}

	ret = media_entity_pads_init(&vdev->entity, 1, &stream->pad);
	if (ret < 0) {
		v4l2_err(&rt->v4l2_dev, "Failed to init media pad\n");
		return ret;
	}

	ret = video_register_device(vdev, VFL_TYPE_VIDEO, -1);
	if (ret < 0) {
		v4l2_err(&rt->v4l2_dev, "Failed to register video device\n");
		return ret;
	}

	v4l2_info(&rt->v4l2_dev, "Device '%s-%d' registered as /dev/video%d\n",
		  MVDU_MODULE_NAME, stream->idx, vdev->num);

	return 0;
}

static int mvdu_streams_register(struct mvdu_data *rt)
{
	int ret, i;

	for (i = 0; i < MAX_STREAMS; i++) {
		ret = mvdu_stream_register(rt, i);
		if (ret)
			return ret;
	}

	return ret;
}

#ifdef CONFIG_MEDIA_CONTROLLER
static const struct media_device_ops m2m_media_ops = {
	.link_notify = v4l2_pipeline_link_notify,
};

static int mvdu_init_media_device(struct device *dev, struct mvdu_data *rt)
{
	rt->mdev.dev = dev;
	strscpy(rt->mdev.model, "mvdu", sizeof(rt->mdev.model));
	strscpy(rt->mdev.bus_info, "platform:mvdu", sizeof(rt->mdev.bus_info));
	media_device_init(&rt->mdev);
	rt->mdev.ops = &m2m_media_ops;
	rt->v4l2_dev.mdev = &rt->mdev;

	return 0;
}
#endif

static int mvdu_gpio_adv7511_powerdown(struct device *dev, struct mvdu_data *rt,
				       bool active)
{
	struct gpio_desc *desc;

	desc = devm_gpiod_get(dev, "hdmio-pd", GPIOD_ASIS);
	if (IS_ERR(desc))
		return 0;

	if (gpiod_get_direction(desc) == 1)
		gpiod_direction_output(desc, 0);

	gpiod_set_value_cansleep(desc, active);
	gpiod_put(desc);

	return 0;
}

static irqreturn_t mvdu_adv7511_irq_thread(int irq, void *arg)
{
	struct mvdu_data *rt = (struct mvdu_data *)arg;

	v4l2_subdev_call(rt->sd, core, interrupt_service_routine, 0, NULL);

	return IRQ_HANDLED;
}

static int mvdu_gpio_adv7511_irq_setup(struct device *dev, struct mvdu_data *rt)
{
	struct gpio_desc *desc;
	int irq, ret;

	desc = devm_gpiod_get(dev, "hdmio-int", GPIOD_IN);
	if (IS_ERR(desc)) {
		dev_err(dev, "mvdu: adv7511 irq get failed\n");
		return -1;
	}

	irq = gpiod_to_irq(desc);
	if (irq < 0) {
		dev_err(dev, "mvdu: adv7511 gpiod to irq failed\n");
		return -1;
	}

	ret = devm_request_threaded_irq(dev, irq, NULL, mvdu_adv7511_irq_thread,
					IRQF_TRIGGER_FALLING | IRQF_ONESHOT,
					"hdmi-int", rt);
	if (ret) {
		dev_err(dev,
			"mvdu: adv7511 irq thread request failed, ret %d\n",
			ret);
		return -1;
	}

	return 0;
}

static int mvdu_adv7511_init(struct device *dev, struct mvdu_data *rt)
{
	struct device_node *np = NULL;
	struct i2c_adapter *i2c_adv7511 = NULL;
	struct device_node *dn = dev->of_node;
	int i;

	np = of_parse_phandle(dn, "hdmio-i2c-bus", 0);
	if (np) {
		i2c_adv7511 = of_find_i2c_adapter_by_node(np);
		of_node_put(np);
	} else {
		dev_dbg(dev, "no hdmio bus configured\n");
		return 1;
	}

	mvdu_gpio_adv7511_powerdown(dev, rt, true);
	mvdu_gpio_adv7511_irq_setup(dev, rt);
	mvdu_gpio_adv7511_powerdown(dev, rt, false);

	if (i2c_adv7511) {
		adv7511_info.of_node = dn;
		rt->sd = v4l2_i2c_new_subdev_board(&rt->v4l2_dev, i2c_adv7511,
						   &adv7511_info, NULL);
	} else {
		dev_warn(dev, "failed to retrieve hdmio i2c adapter\n");
		return -EAGAIN;
	}

	if (rt->sd == NULL) {
		dev_warn(dev, "ADV7511 not yet available ...\n");
		return -EAGAIN;
	}

	for (i = 0; i < MAX_STREAMS; i++)
		rt->stream[i].sd = rt->sd;

	return 0;
}

static int mvdu_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct mvdu_data *rt;
	int ret = 0;
	const char *name = dev_name(dev);

	dev_info(dev, "probing mvdu-v4l...\n");

	rt = kzalloc(sizeof(struct mvdu_data), GFP_ATOMIC);
	if (rt == NULL)
		return -ENOMEM;

	platform_set_drvdata(pdev, rt);

	ret = mvdu_alloc_ressources(pdev);
	if (ret) {
		dev_err(dev, "alloc ressources failed\n");
		goto err_subdevs;
	}

	ret = v4l2_device_register(&pdev->dev, &rt->v4l2_dev);
	if (ret) {
		dev_err(dev, "v4l2_device_register failed\n");
		goto err_subdevs;
	}

	ret = mvdu_init(pdev, rt);
	if (ret < 0) {
		dev_err(dev, "mvdu_init failed\n");
		goto err_subdevs;
	}

	mvdu_get_of_config(pdev, rt);

	ret = mvdu_adv7511_init(dev, rt);
	if (ret < 0) {
		if (ret == -EAGAIN) {
			dev_warn(dev, "waiting for ADV7511 ...\n");
			ret = -EPROBE_DEFER;
		}
		goto err_register;
	}

	if (!ret) {
		ret = mvdu_subdevs_init(rt);
		if (ret) {
			dev_warn(dev, "error %d init subdevice\n", ret);
			goto err_register;
		}
	}

#ifdef CONFIG_MEDIA_CONTROLLER
	mvdu_init_media_device(dev, rt);
#endif

	rt->v4l2_dev.notify = mvdu_notify;
	rt->dev = rt->v4l2_dev.dev;

	ret = mvdu_streams_register(rt);
	if (ret) {
		dev_err(dev, "error %d registering device node\n", ret);
		goto err_register;
	}

#ifdef CONFIG_MEDIA_CONTROLLER
	ret = media_device_register(&rt->mdev);
	if (ret) {
		dev_err(dev, "error %d registering media device\n", ret);
		goto err_register;
	}
#endif

	return 0;

err_register:
	v4l2_device_unregister(&rt->v4l2_dev);

err_subdevs:
	mvdu_free_ressources(pdev);
	kfree(rt);

	return ret;
}

static int mvdu_stream_remove(struct mvdu_stream *stream)
{
	if (!video_is_registered(&stream->vdev))
		return 0;

	video_unregister_device(&stream->vdev);
	media_entity_cleanup(&stream->vdev.entity);

	return 0;
}

static int mvdu_remove(struct platform_device *pdev)
{
	struct mvdu_data *rt = platform_get_drvdata(pdev);
	int i;

	if (rt == NULL)
		return 0;

	for (i = 0; i < MAX_STREAMS; i++)
		mvdu_stream_remove(&rt->stream[i]);

#ifdef CONFIG_MEDIA_CONTROLLER
	media_device_unregister(&rt->mdev);
#endif

	v4l2_device_unregister(&rt->v4l2_dev);
	mvdu_free_ressources(pdev);
	kfree(rt);

	return 0;
}

static struct of_device_id mvdu_of_match[] = {
	{
		.compatible = "dct,dct-mvdu-v4l",
	},
	{},
};
MODULE_DEVICE_TABLE(of, mvdu_of_match);

static struct platform_driver mvdu_driver = {
    .probe = mvdu_probe,
    .remove = mvdu_remove,
    .driver =
        {
            .name = MVDU_MODULE_NAME,
            .owner = THIS_MODULE,
            .of_match_table = of_match_ptr(mvdu_of_match),
        },
};

static int __init mvdu_init_module(void)
{
	int ret;

	ret = platform_driver_register(&mvdu_driver);
	if (ret)
		return ret;

	return ret;
}

static void __exit mvdu_exit_module(void)
{
	platform_driver_unregister(&mvdu_driver);
}

module_init(mvdu_init_module);
module_exit(mvdu_exit_module);

MODULE_DESCRIPTION("DCT MVDU V4L driver");
MODULE_LICENSE("GPL v2");
