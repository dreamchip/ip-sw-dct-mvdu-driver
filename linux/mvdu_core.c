/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#undef DEBUG

#include <linux/clk.h>
#include <linux/platform_device.h>

#include "mvdu_v4l.h"

#include "gdp_memmap.h"
#include "mvdu_cku.h"
#include "mvdu_functions.h"
#include "mvdu_memmap.h"
#include "mvdu_structures.h"

#include "gdp_ctrl_reg.h"
#include "mvdu_dif.h"
#include "mvdu_fx_ctrl_reg.h"
#include "mvdu_fx_dif_reg.h"
#include "mvdu_fx_mi_reg.h"
#include "mvdu_fx_osd_reg.h"
#include "mvdu_gdp.h"
#include "mvdu_mi.h"
#include "mvdu_top_reg.h"

static const struct mvdu_dif def_display_info = {

	/* timing mode of display interface */
	.timing_mode = TM_YC_EXSYNC_1REPL,

	.act_width = 1920,
	.act_height = 1080,
	.h_front_porch = 88,
	.h_sync = 44,
	.h_back_porch = 148,
	.v_front_porch = 4,
	.v_sync = 5,
	.v_back_porch = 36,

	/* default value for Y */
	.def_y = 0xA0,
	/* default value for Cb */
	.def_cb = 0xB0,
	/* default value for Cr */
	.def_cr = 0xC0,
};

static struct mvdu_data *mvdu_get_priv(struct platform_device *pdev)
{
	if (pdev == NULL)
		return NULL;

	return platform_get_drvdata(pdev);
}

static void mvdu_init_mif(struct mvdu_mif *mif)
{
	mif->color_mode_422 = true;
	mif->little_endian = true;
	mif->small_packet_en = false;
}

static irqreturn_t fx_irq_handler(int irq, void *data)
{
	struct mvdu_data *priv = data;
	uint32_t mis, i;

	mis = mvdu_read(priv->fx_base + MVDU_RIS_REG);

	if (mis & DIF_DISP_FINISH_MSK_MASK) {
		struct mvdu_stream *s;
		s = &priv->stream[0];
		for (i = 0; i < MAX_STREAMS; i++, s++) {
			if (s->enabled && s->irq_handler)
				s->irq_handler(i, s->irq_ctx);
		}
		priv->stat.disp_finish++;
	}
	if (mis & DIF_LINE_TRESHOLD_MSK_MASK)
		priv->stat.line_treshold++;
	if (mis & DIF_VID1_UFLW_MSK_MASK)
		priv->stat.stream[VID].uflw++;
	if (mis & DIF_PIP_UFLW_MSK_MASK)
		priv->stat.stream[PIP].uflw++;
	if (mis & DIF_OSD1_UFLW_MSK_MASK)
		priv->stat.stream[OSD].uflw++;
	if (mis & VID1_MEM_URGENT_MSK_MASK)
		priv->stat.stream[VID].mem_urgent++;
	if (mis & PIPIN_MEM_URGENT_MSK_MASK)
		priv->stat.stream[PIP].mem_urgent++;
	if (mis & STOP_VTG_FINISH_MSK_MASK)
		priv->stat.stop_vtg_finish++;

	mvdu_write(mis, priv->fx_base + MVDU_ICR_REG);

	return IRQ_HANDLED;
}

static irqreturn_t gdp_irq_handler(int irq, void *data)
{
	struct mvdu_data *priv = data;
	uint32_t mis;

	mis = mvdu_read(priv->gdp_base + GDP_RIS_REG);

	if (mis & BIT(6))
		priv->stat.stream[OSD].mem_urgent++;

	mvdu_write(mis, priv->gdp_base + GDP_ICR_REG);

	return IRQ_HANDLED;
}

int mvdu_get_reg(struct platform_device *pdev, unsigned int addr)
{
	struct mvdu_data *priv = mvdu_get_priv(pdev);

	return mvdu_read(priv->fx_base + addr);
}

int mvdu_set_reg(struct platform_device *pdev, unsigned int addr,
		 unsigned int val)
{
	struct mvdu_data *priv = mvdu_get_priv(pdev);

	mvdu_write(val, priv->fx_base + addr);

	return 0;
}

static void mvdu_video_base_update(struct mvdu_data *priv, uintptr_t y_base,
				   uintptr_t c_base)
{
	mvdu_mi_write(y_base, priv->fx_base + MI_V1_Y_BA_REG);
	mvdu_mi_write(c_base, priv->fx_base + MI_V1_C_BA_REG);
	mvdu_config_mif_video(priv->fx_base, &priv->stream[VID].mif);
	mvdu_dif_frame_start(priv->fx_base, false);
}

static void mvdu_pip_base_update(struct mvdu_data *priv, uintptr_t y_base,
				 uintptr_t c_base)
{
	mvdu_mi_write(y_base, priv->fx_base + MI_PIP_IN_Y_BA_REG);
	mvdu_mi_write(c_base, priv->fx_base + MI_PIP_IN_C_BA_REG);
	mvdu_config_mif_pip(priv->fx_base, &priv->stream[PIP].mif);
}

static void mvdu_osd_base_update(struct mvdu_data *priv, uintptr_t rgb_base,
				 uintptr_t zone_base_phys,
				 uintptr_t zone_base_virt)
{
	mvdu_write(DIF_OSD1_UFLW_CLR_MASK,
		   priv->fx_base + MVDU_CTRL_BASE + MVDU_ICR_REG);

	if (!zone_base_phys || !zone_base_virt)
		return;

	mvdu_config_zones(&priv->stream[OSD].mif, zone_base_virt, rgb_base);
	mvdu_config_mif_osd(priv->gdp_base, zone_base_phys);
}

int mvdu_sched_next(struct platform_device *pdev, uint32_t idx,
		    struct mvdu_buffer *buf)
{
	struct vb2_v4l2_buffer *vbuf;
	struct mvdu_data *priv = mvdu_get_priv(pdev);
	uintptr_t base_phys[2] = { 0, 0 };
	uintptr_t base_virt[2] = { 0, 0 };
	uint32_t plane_size[2] = { 0, 0 };
	int i;

	if (priv == NULL)
		return -EINVAL;

	if (buf == NULL)
		return -EINVAL;

	if (idx >= MAX_STREAMS)
		return -EINVAL;

	vbuf = &buf->vb;

	for (i = 0; i < vbuf->vb2_buf.num_planes; i++) {
		base_phys[i] = vb2_dma_contig_plane_dma_addr(&vbuf->vb2_buf, i);
		if (!IS_ALIGNED(base_phys[i], 8))
			printk("%s: base[%d] not aligned, %jx\n", __func__, i,
			       base_phys[i]);
		base_virt[i] = (uintptr_t)vb2_plane_vaddr(&vbuf->vb2_buf, i);
		plane_size[i] = vb2_plane_size(&vbuf->vb2_buf, i);
		if (priv->fpga)
			base_phys[i] |= 0x80000000;
	}

	if (idx == VID)
		mvdu_video_base_update(priv, base_phys[0], base_phys[1]);
	else if (idx == PIP)
		mvdu_pip_base_update(priv, base_phys[0], base_phys[1]);
	else if (idx == OSD) {
		if (buf->zb) {
			base_virt[1] = buf->zb->base_virt;
			base_phys[1] = buf->zb->base_phys;
			if (priv->fpga)
				base_phys[1] |= 0x80000000;
		}
		mvdu_osd_base_update(priv, base_phys[0], base_phys[1],
				     base_virt[1]);
	}

	priv->stat.stream[idx].frm++;

	return 0;
}

static int mvdu_config_clock(struct platform_device *pdev)
{
	int ret;
	struct mvdu_data *priv = mvdu_get_priv(pdev);

	dev_err(&pdev->dev, "%s set pixel clock %d Mhz\n", __func__,
		priv->dif.pixelclock);

	if (priv->pixelclock == priv->dif.pixelclock)
		return 0;

	priv->pixelclock = priv->dif.pixelclock;

	if (IS_ERR_VALUE(priv->pclk))
		return -EINVAL;

	// TODO: unprepare/prepare_enable might not be needed
	//       since clock driver supports setting rate
	//       while clock is active
	clk_disable_unprepare(priv->pclk);

	ret = clk_set_rate(priv->pclk, priv->dif.pixelclock);
	if (ret < 0)
		dev_err(&pdev->dev, "%s can't set pixel clock %d Mhz\n",
			__func__, priv->dif.pixelclock);

	ret = clk_prepare_enable(priv->pclk);
	if (ret < 0)
		dev_err(&pdev->dev, "%s can't enable pixel clock\n", __func__);

	return 0;
}

int mvdu_enabled(struct platform_device *pdev)
{
	struct mvdu_data *priv = mvdu_get_priv(pdev);

	return !!(priv->stream[0].enabled | priv->stream[1].enabled |
		  priv->stream[2].enabled);
}

bool mvdu_last_stream(struct platform_device *pdev)
{
	struct mvdu_data *priv = mvdu_get_priv(pdev);
	uint32_t i, cnt;

	for (i = 0, cnt = 0; i < MAX_STREAMS; i++) {
		if (priv->stream[i].enabled)
			cnt++;
	}

	return cnt == 1;
}

static int mvdu_enable_cku(struct platform_device *pdev, uint32_t idx)
{
	struct cku_cfg cku_data;
	struct mvdu_data *priv = mvdu_get_priv(pdev);

	cku_data.enable_insular_padding = 0x01;
	cku_data.enable_interpol_padding = 0x01;
	cku_data.cku_enable = 0x01;
	cku_data.cr_key_max = 0x92; // 146
	cku_data.cb_key_max = 0x91; // 145
	cku_data.y_key_max = 0x90; // 144
	cku_data.cr_key_min = 0x42; // 66
	cku_data.cb_key_min = 0x41; // 65
	cku_data.y_key_min = 0x40; // 64
	cku_data.alpha_no_key = 0xFF;
	cku_data.alpha_key = 0x00;
	cku_data.alpha_insular_out = 0xE5;
	cku_data.alpha_insular_in = 0xD0;
	cku_data.alpha_ipol_out = 0xD5;
	cku_data.alpha_ipol_in = 0xC0;

	cku_data.alpha_no_key = priv->stream[idx].mif.alpha;

	cku_config(priv->fx_base, &cku_data);
	cku_enable(priv->fx_base, 1);

	return 0;
}

static void mvdu_init_top(struct platform_device *pdev)
{
	struct mvdu_data *priv = mvdu_get_priv(pdev);

	mvdu_write(priv->top.omux_ctrl, priv->top_base + MVDU_OMUX_CTRL_REG);
}

static int mvdu_reset_stat_chan(struct platform_device *pdev, uint32_t idx)
{
	struct mvdu_data *priv = mvdu_get_priv(pdev);

	if (idx >= MAX_STREAMS)
		return -EINVAL;

	memset(&priv->stat.stream[idx], 0x00, sizeof(struct mvdu_stream_stat));

	return 0;
}

static int mvdu_prepare_zone_bufs(struct platform_device *pdev)
{
	struct mvdu_data *priv = mvdu_get_priv(pdev);
	uintptr_t virt, phys;
	int i;

	INIT_LIST_HEAD(&priv->zone_buf_list);

	if (priv->zone_mem.vaddr == NULL)
		return -ENOMEM;

	virt = (uintptr_t)priv->zone_mem.vaddr;
	phys = priv->zone_mem.paddr;

	for (i = 0; i < MVDU_ZONE_CHUNK_NUM; i++) {
		priv->zone_buf[i].base_virt = virt;
		priv->zone_buf[i].base_phys = phys;

		list_add_tail(&priv->zone_buf[i].list, &priv->zone_buf_list);

		phys += MVDU_ZONE_CHUNK_SIZE;
		virt += MVDU_ZONE_CHUNK_SIZE;
	}

	return 0;
}

int mvdu_start_stream(struct platform_device *pdev, uint32_t idx,
		      struct mvdu_buffer *vbuf)
{
	struct mvdu_data *priv = mvdu_get_priv(pdev);

	if (idx >= MAX_STREAMS)
		return -EINVAL;

	mvdu_reset_stat_chan(pdev, idx);
	mvdu_init_top(pdev);
	mvdu_init_mif(&priv->stream[idx].mif);

	if (!mvdu_enabled(pdev))
		mvdu_config_clock(pdev);

	if (!mvdu_enabled(pdev)) {
		mvdu_config_display(priv->fx_base, &priv->dif);
		mvdu_start_vtg(priv->fx_base);
		mvdu_config_display(priv->fx_base, &priv->dif);
	}

	mvdu_sched_next(pdev, idx, vbuf);

	if (idx == VID)
		mvdu_enable_video(priv->fx_base);
	else if (idx == PIP) {
		mvdu_enable_cku(pdev, idx);
		mvdu_enable_pip(priv->fx_base);
	} else if (idx == OSD)
		mvdu_enable_osd(priv->fx_base, priv->gdp_base);

	if (!mvdu_enabled(pdev))
		mvdu_enable_disp_finish_irq(priv->fx_base);

	priv->stream[idx].enabled = 1;

	return 0;
}

int mvdu_stop_stream(struct platform_device *pdev, uint32_t idx)
{
	struct mvdu_data *priv = mvdu_get_priv(pdev);
	bool last = mvdu_last_stream(pdev);

	if (idx >= MAX_STREAMS)
		return -EINVAL;

	priv->stream[idx].enabled = false;

	if (idx == VID)
		mvdu_disable_video(priv->fx_base);
	else if (idx == PIP)
		mvdu_disable_pip(priv->fx_base);
	else if (idx == OSD)
		mvdu_disable_osd(priv->fx_base, priv->gdp_base);

	if (last) {
		mvdu_disable_disp_finish_irq(priv->fx_base);
		mvdu_stop_vtg(priv->fx_base);
	}

	if (idx == OSD)
		mvdu_prepare_zone_bufs(pdev);

	return 0;
}

int mvdu_register_irq_handler(struct platform_device *pdev, uint32_t idx,
			      int (*irq_handler)(uint32_t idx, void *irq_ctx),
			      void *irq_ctx)
{
	struct mvdu_data *priv = mvdu_get_priv(pdev);

	if (idx >= MAX_STREAMS)
		return -EINVAL;

	priv->stream[idx].irq_handler = irq_handler;
	priv->stream[idx].irq_ctx = irq_ctx;

	return 0;
}

uint32_t mvdu_read(uintptr_t base)
{
	return readl((void __iomem *)base);
}

void mvdu_write(uint32_t val, uintptr_t base)
{
	writel(val, (void __iomem *)base);
}

static int mvdu_alloc_zone_mem(struct platform_device *pdev, uint32_t size)
{
	struct mvdu_data *priv = mvdu_get_priv(pdev);
	dma_addr_t dma_handle;

	priv->zone_mem.vaddr =
		dma_alloc_coherent(&pdev->dev, size, &dma_handle, GFP_KERNEL);
	if (priv->zone_mem.vaddr == NULL)
		return -ENOMEM;
	priv->zone_mem.paddr = dma_handle;

	priv->zone_mem.size = size;
	memset(priv->zone_mem.vaddr, 0, priv->zone_mem.size);

	dev_info(&pdev->dev, "GPD descriptor base: %#x\n",
		 (uint32_t)priv->zone_mem.paddr);

	if (((uint32_t)priv->zone_mem.paddr) & 0x3) {
		dev_err(&pdev->dev, "invalid GPD descriptor alignment: %#x\n",
			(uint32_t)priv->zone_mem.paddr);
		dma_free_coherent(&pdev->dev, priv->zone_mem.size,
				  priv->zone_mem.vaddr, priv->zone_mem.paddr);
		return -EIO;
	}

	mvdu_prepare_zone_bufs(pdev);

	return 0;
}

static void mvdu_free_gpd_desc_mem(struct platform_device *pdev)
{
	struct mvdu_data *priv = mvdu_get_priv(pdev);
	if (priv == NULL)
		return;

	if (priv->zone_mem.vaddr == NULL)
		return;

	dma_free_coherent(&pdev->dev, priv->zone_mem.size, priv->zone_mem.vaddr,
			  priv->zone_mem.paddr);
}

static irq_handler_t handler[] = { fx_irq_handler, gdp_irq_handler };

int mvdu_alloc_ressources(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct device_node *dn = dev->of_node;
	struct mvdu_data *priv = mvdu_get_priv(pdev);
	struct resource *res;
	int irq, ret, i;
	uint32_t val;

	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	priv->fx_base = (uintptr_t)devm_ioremap_resource(dev, res);
	if (IS_ERR_VALUE(priv->fx_base))
		return (long)(priv->fx_base);

	res = platform_get_resource(pdev, IORESOURCE_MEM, 1);
	priv->gdp_base = (uintptr_t)devm_ioremap_resource(dev, res);
	if (IS_ERR_VALUE(priv->gdp_base))
		return (long)(priv->gdp_base);

	res = platform_get_resource(pdev, IORESOURCE_MEM, 2);
	priv->top_base = (uintptr_t)devm_ioremap_resource(dev, res);
	if (IS_ERR_VALUE(priv->top_base))
		return (long)(priv->top_base);

	priv->pdev = pdev;

	/* get and enable mvdu IP clock: */
	priv->mclk = devm_clk_get_enabled(dev, "mvdu-clk");
	if (IS_ERR_VALUE(priv->mclk)) {
		dev_warn(dev, "get mvdu clock failed\n");
	}

	/* get and enable pixelclock: */
	priv->pclk = devm_clk_get_enabled(dev, "mvdu-pclk");
	if (IS_ERR_VALUE(priv->pclk)) {
		dev_warn(dev, "get pixel clock failed\n");
	}

	ret = mvdu_alloc_zone_mem(pdev, MVDU_ZONE_BUFFER_SIZE);
	if (ret) {
		dev_err(dev, "Can't allocate zone memory %d\n", ret);
		return ret;
	}

	memcpy(&priv->dif, &def_display_info, sizeof(struct mvdu_dif));

	ret = of_property_read_u32(dn, "timing-mode", &priv->dif.timing_mode);
	if (ret < 0)
		priv->dif.timing_mode = TM_YC_EXSYNC_1REPL;

	dev_dbg(&pdev->dev, "%s: timing-mode %d\n", __func__,
		priv->dif.timing_mode);

	val = mvdu_read(priv->top_base + MVDU_OMUX_CTRL_REG) & ~0x30;
	if (of_property_read_bool(dn, "mipi-enable")) {
		val |= 0x20;
	} else {
		val &= ~0x20;
	}
	if (of_property_read_bool(dn, "tv6xy-enable")) {
		val |= 0x10;
	} else {
		val &= ~0x10;
	}
	if (of_property_read_bool(dn, "cb-cr-swap-enable")) {
		val |= 0x04;
	} else {
		val &= ~0x4;
	}
	if (of_property_read_bool(dn, "luma-chroma-swap-enable")) {
		val |= 0x02;
	} else {
		val &= ~0x2;
	}
	if (of_property_read_bool(dn, "video-out-bypass-enable")) {
		val |= 0x01;
	} else {
		val &= ~0x1;
	}
	priv->top.omux_ctrl = val;

	if (of_property_read_bool(dn, "fpga-enable"))
		priv->fpga = true;

	dev_dbg(&pdev->dev, "%s: fpga-enable %d\n", __func__, priv->fpga);

	for (i = 0; i < ARRAY_SIZE(handler); i++) {
		irq = platform_get_irq(pdev, i);
		if (irq < 0) {
			dev_warn(dev, "missing %s, idx %d definition\n",
				 dev_name(dev), i);
			continue;
		}

		ret = devm_request_irq(dev, irq, handler[i], 0, dev_name(dev),
				       priv);
		if (ret < 0) {
			dev_err(dev, "failed to request %s, idx %d, ret %d\n",
				dev_name(dev), irq, ret);
			goto mvdu_get_irq_fail;
		}
	}

	return 0;

mvdu_get_irq_fail:

	mvdu_free_gpd_desc_mem(pdev);

	return -1;
}

int mvdu_free_ressources(struct platform_device *pdev)
{
	int i;

	for (i = 0; i < 2; i++)
		mvdu_stop_stream(pdev, i);

	mvdu_free_gpd_desc_mem(pdev);

	return 0;
}
